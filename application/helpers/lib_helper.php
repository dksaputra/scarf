<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('pre')) {
    function pre($data = array())
    {
        echo "<PRE>";
        print_r($data);
        echo "</PRE>";
        exit;
    }
}

if (!function_exists('dateSekarang')) {
    function dateSekarang($act = 1, $param = FALSE)
    {
        if ($act == 1) {
            return date("Y-m-d H:i:s");
        } else if ($act == 2) {
            return date("Y-m-d");
        } else if ($act == 3) {
            return date("d M Y H:i", strtotime($param));
        } else if ($act == 4) {
            return date("d M Y", strtotime($param));
        } else if ($act == 5) {
            return date("Y/m/d");
        }
    }
}


if (!function_exists('valCheck')) {
    function valCheck($val)
    {
        return ($val == 1) ? 'checked="checked"' : '';
    }
}

if (!function_exists('susun_tanggal')) {
    function susun_tanggal($act, $format, $param)
    {
        $output = "";
        if ($act == 1) { //format datetime dari hasil masking input[texts] 
            $pecah = explode($format, substr($param, 0, 10));
            $output = "{$pecah[2]}-{$pecah[1]}-{$pecah[0]} " . substr($param, 11, 14) . ':00';
        }
        return $output;
    }
}

function dateformatindo($vardate, $type = '')
{
    $hari = array('Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu');
    $bulan = array(1 => 'Januari', 2 => 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
    $dywk = date('w', strtotime($vardate));
    $dywk = $hari[$dywk];
    $dy = date('j', strtotime($vardate));
    $d = date('d', strtotime($vardate));
    $mth = date('n', strtotime($vardate));
    $m = date('m', strtotime($vardate));
    $mt = date('M', strtotime($vardate));
    $y = date('y', strtotime($vardate));
    $mth = $bulan[$mth];
    $yr = date('Y', strtotime($vardate));
    $hr = date('H', strtotime($vardate));
    $mi = date('i', strtotime($vardate));

    if ($type == '') {
        return $dywk . ', ' . $dy . ' ' . $mth . ' ' . $yr . '';
    } elseif ($type == '1') {
        return $dywk . ', ' . $dy . ' ' . $mth . ' ' . $yr . ' | ' . $hr . ':' . $mi . ' WIB';
    } elseif ($type == '2') {
        return $dy . ' ' . $mth . ' ' . $yr . '';
    } elseif ($type == '3') {
        return $dywk . ', ' . $dy . ' ' . $mth . ' ' . $yr . ' ' . $hr . ':' . $mi;
    } elseif ($type == '4') {
        return $dywk . ', ' . $dy . ' ' . $mth . ' ' . $yr;
    } elseif ($type == '5') {
        return $dy . '/' . $mt . '/' . $yr . ' | ' . $hr . ':' . $mi . ' WIB';
    } elseif ($type == 'd') {
        return $d;
    } elseif ($type == 'y/m/d') {
        return $yr . '/' . $m . '/' . $d;
    } elseif ($type == 'my') {
        return $mt . ' ' . $y;
    }
}

function cleanteks($teks)
{
    $find = array('|[_]{1,}|', '|[ ]{1,}|', '|[^0-9A-Za-z\-.]|', '|[-]{2,}|', '|[.]{2,}|');
    $replace = array('.', '.', '', '.', '.');
    $newname = strtolower(preg_replace($find, $replace, $teks));
    return $newname;
}