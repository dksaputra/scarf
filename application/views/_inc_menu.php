<ul id="nav">
    <li><a href="<?php echo site_url('') ?>">Home</a></li>
    <li><a href="<?php echo site_url('employee/index') ?>">Employee</a>
        <?if ($this->session->userdata('type')==1){?>
            <ul>
                <li><a href="<?php echo site_url('employee/create') ?>">Input Employee</a></li>
                <li><a href="<?php echo site_url('employee/search') ?>">Search Employee</a></li>
            </ul>
        <?}?>
    </li>
    <li><a href="<?php echo site_url('article/index') ?>">Article</a>
        <?if ($this->session->userdata('type')==6){?>
            <ul>
                <li><a href="<?php echo site_url('article/search') ?>">Search Article</a></li>
                <li><a href="<?php echo site_url('article/approval') ?>">Approval</a></li>
            </ul>
        <?}else if ($this->session->userdata('type')==2){?>
            <ul>
                <li><a href="<?php echo site_url('article/create') ?>">Input Article</a></li>
                <li><a href="<?php echo site_url('article/search') ?>">Search Article</a></li>
                <li><a href="<?php echo site_url('article/myarticle') ?>">My Article</a></li>
            </ul>
        <?}else{?>
            <ul>
                <li><a href="<?php echo site_url('article/create') ?>">Input Article</a></li>
                <li><a href="<?php echo site_url('article/search') ?>">Search Article</a></li>
                <li><a href="<?php echo site_url('article/myarticle') ?>">My Article</a></li>
                <li><a href="<?php echo site_url('article/approval') ?>">Approval</a></li>
            </ul>
        <?}?>
    <li><a href="<?php echo site_url('layout/index') ?>">Layout</a>
    <li><a href="<?php echo site_url('account/change_password') ?>">Account</a></li>
    <li><a href="<?php echo site_url('account/logout') ?>">Logout</a></li>
</ul>

