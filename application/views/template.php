<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-gb" lang="en-gb">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="robots" content="index, follow">
    <meta name="keywords" content="Sistem Informasi Manajemen Pengetahuan">
    <meta name="description" content="Sistem Informasi  Manajemen Pengetahuan">
    <meta name="generator" content="Sistem Informasi Buku  Manajemen Pengetahuan">
    <title><?php echo $this->config->item('title'); ?></title>
    <link href="<?php echo css_path('system.css'); ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo css_path('general.css'); ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo css_path('style.css'); ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo css_path('template.css'); ?>" rel="stylesheet" type="text/css"/>
    <?php echo js('jquery-1.2.3.min.js'); ?>
    <?php echo js('menu.js'); ?>
    <script language="javascript" type="text/javascript">
        function iFrameHeight() {
            var h = 0;
            if (!document.all) {
                h = document.getElementById('blockrandom').contentDocument.height;
                document.getElementById('blockrandom').style.height = h + 60 + 'px';
            } else if (document.all) {
                h = document.frames('blockrandom').document.body.scrollHeight;
                document.all.blockrandom.style.height = h + 20 + 'px';
            }
        }
    </script>
    <style type="text/css">
        .TFtable {
            width: 100%;
            border-collapse: collapse;
        }

        .TFtable td {
            padding: 7px;
            border: #4e95f4 1px solid;
        }

        /* provide some minimal visual accomodation for IE8 and below */
        .TFtable tr {
            background: #b8d1f3;
        }

        /*  Define the background color for all the ODD background rows  */
        .TFtable tr:nth-child(odd) {
            background: #b8d1f3;
        }

        /*  Define the background color for all the EVEN background rows  */
        .TFtable tr:nth-child(even) {
            background: #dae5f4;
        }
    </style>
</head>
<body>
<a name="up" id="up"></a>
<div class="center" align="center">
    <div id="top">
        <div id="header">
            <div id="logo"><a href="/"></a></div>
        </div>
        <div id="tabarea">
            <table class="pill" cellpadding="0" cellspacing="0">
                <tbody>
                    <tr>
                        <td class="pill_m">
                            <div id="pillmenu"></div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div id="wrapper">
        <div id="wrapper_r">
            <div id="whitebox_m">
                <div id="area">
                    <div id="leftcolumn" style="float: left;">
                        <div class="moduletable_menu">
                            <h3>Calendar</h3>
                            <?php $this->load->view('_inc_calendar'); ?>
                        </div>
                        <div class="moduletable_menu">
                            <h3>Time</h3>
                            <?php $this->load->view('_inc_time'); ?>
                        </div>
                    </div>
                    <div id="maincolumn">
                        <div class="nopad">
                            <div class="contentpane">
                                <div class="componentheading">SCARF MAGAZINE</div>
                                <?php if($this->session->userdata('uid') != ""): ?>
                                    <?php $this->load->view('_inc_menu'); ?>
                                <?php endif; ?>
                                <div class="clr"></div>
                                <br/>
                                <p>
                                    <?php echo $this->session->flashdata("info"); ?>
                                </p>
                                <?php echo $contents; ?>
                            </div>
                        </div>
                    </div>
                    <div class="clr"></div>
                </div>
            </div>
        </div>
        <div id="footer">
            <div id="footer_l">
                <div id="footer_r">
                    <p style="padding: 4px 25px; float: left;">
                    </p>
                    <p style="padding: 4px 25px; float: right;">
                        Valid <a href="http://validator.w3.org/check/referer">XHTML</a> and <a href="http://jigsaw.w3.org/css-validator/check/referer">CSS</a>.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="sgf"></div>
</body>
</html>
