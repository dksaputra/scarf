<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$route['default_controller'] = "employee/index";
$route['404_override'] = 'employee/index';

$route['logout'] = 'login/logout';
//$route['report/detail/(:any)'] = 'search/report_detail/$1';
/* End of file routes.php */
/* Location: ./application/config/routes.php */
