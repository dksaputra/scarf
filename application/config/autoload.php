<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$autoload['packages'] = array(APPPATH . 'third_party');
$autoload['libraries'] = array('lib', 'asset', 'template', 'database', 'session', 'encrypt');
$autoload['helper'] = array('url', 'form', 'asset_helper', 'lib');
$autoload['config'] = array('config');
$autoload['language'] = array();
$autoload['model'] = array();

/* End of file autoload.php */
/* Location: ./application/config/autoload.php */

