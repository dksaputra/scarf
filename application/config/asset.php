<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$config['asset_dir'] = APPPATH_URI;
$config['asset_url'] = APPPATH_URI;
$config['asset_img_dir'] = 'img';
$config['asset_js_dir'] = 'js';
$config['asset_css_dir'] = 'css';