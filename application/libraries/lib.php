<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Lib {

    function __construct() {
        $this -> _ci = &get_instance();
    }

    public function generateMenu() {
        $arrmenu = array();
        return $arrmenu;
    }

    public function checkAuth($module = FALSE) {
        $CI = &get_instance();
        if (!$CI -> session -> userdata('sess_user')) {
            redirect('login');
        } else {
            if ($module != 'notdefine') {
                if ($CI -> session -> userdata($module) == 0) {
                    redirect('restricted');
                }
            }
        }
    }

    public function addMenu() {
        $CI = &get_instance();
        $menu = '';
        if ($CI -> session -> userdata('isAdmin') == 1)
            $menu = 'admin';

        return $menu;
    }

    public function upload($nameform) {
        $output = array();
        $nf = time();
        $directory = $this -> _upload_directory_file($nf);
        $upload_path = './uploads/' . $directory . '/' . $nf;

        $this -> _create_directory_file($upload_path);
        $config['upload_path'] = $upload_path . '/';
        $config['file_name'] = $nf;
        $config['allowed_types'] = 'xls|xlsx';
        $config['max_size'] = '3072';

        $CI = &get_instance();
        $CI -> load -> library('upload', $config);
        $upload = $CI -> upload -> do_upload($nameform);

        if ($upload == FALSE) {
            $output = array('status' => 0);
        } else {
            $data = $CI -> upload -> data();
            $output = array(
                'status' => 1,
                'filename' => $data['raw_name'],
                'filetype' => $data['file_ext'],
            );
        }
        return $output;
    }

    private function _delete_file($filename, $type) {
        $directory = $this -> _upload_directory_file($filename);
        $dir = './uploads/' . $directory . '/' . $filename . '/';
        $file = $dir . $filename . $type;
        if (is_file($file)) {
            unlink($file);
        }
    }

    private function _upload_directory_file($time) {
        return date("Y/m/d", $time);
    }

    private function _create_directory_file($dirName, $rights = 0774) {
        $dirs = explode('/', $dirName);
        $dir = '';
        foreach ($dirs as $part) {
            $dir .= $part . '/';
            if (!is_dir($dir) && strlen($dir) > 0)
                mkdir($dir, $rights);
        }
    }

    public function sendCURL($url, $data) {
        $curl_handle = curl_init();
        curl_setopt($curl_handle, CURLOPT_URL, $url);
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl_handle, CURLOPT_POST, 1);
        curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $data);
        $result = curl_exec($curl_handle);
        return $result;
    }

    public function getFileName($filename, $filetype) {
        $directory = $this -> _upload_directory_file($filename);
        $dir = './uploads/' . $directory . '/' . $filename . '/';
        $file = $dir . $filename . $filetype;
        return $file;
    }

}
