<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User_model extends CI_Model {

    private $table = 'tb_users';

    public function __construct() {
        parent::__construct();
    }

    public function totalData() {
        return $this -> db -> count_all($this -> table);
    }

    public function listingData($limit = 1, $offset = 0) {
        $this -> db -> select("*", FALSE);
        return $this -> db -> get($this -> table, $limit, $offset);
    }

    public function detailData($act, $filter = array()) {
        try {
            if ($act == 'detail') {
                $query = $this -> db -> select("*") -> get_where($this -> table, array("id" => $filter['id']));
            }

            if ($query === FALSE)
                throw new Exception();

            return $query -> result();
        } catch (Exception $e) {
            $errNo = $this -> db -> _error_number();
            return $errNo;
        }
    }

    public function processData($act, $param = array(), $filter = array()) {
        try {
            switch ($act) :
                case "insert" :
                    $query = $this -> db -> insert($this -> table, $param);
                    break;
                case "update" :
                    $query = $this -> db -> update($this -> table, $param, array("id" => $filter['id']));
                    break;
                case "delete" :
                    $query = $this -> db -> delete($this -> table, array("id" => $filter['id']));
                    break;
            endswitch;

            if ($query === FALSE)
                throw new Exception();

            return true;
        } catch (Exception $e) {
            $errNo = $this -> db -> _error_number();
            return $errNo;
        }
    }

}
