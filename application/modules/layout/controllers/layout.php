<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Layout extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        if($this->session->userdata('uid') == ""){
            redirect('/account/login', 'refresh');
        }
        $this->load->model("layout_model");
    }

    public function index()
    {
        $count = 0;
        $hal = $this->input->get('page');
        $jlh = $this->input->get('count');

        if ($jlh <= 0) {
            $count = 0;
        } else {
            $count = $jlh;
        }

        $c = $this->layout_model->total_articles();
        $d = floor(($c - 1) / 30) + 1;

        if ($hal <= 0) {
            $hal = 1;
        }
        if ($hal > $d) {
            $hal = $d;
        }

        $batas = (30 * ($hal - 1));
        if ($batas < 0) {
            $batas2 = 0;
        } else {
            $batas2 = $batas;
        }

        $data = array();
        $data['hal'] = $hal;
        $data['c'] = $c;
        $data['d'] = $d;
        $data['count'] = $count;
        $data['datas'] = $this->layout_model->listing_articles();
        $this->template->load('template', 'index_view', $data);
    }

    public function process()
    {
        $id = $this->input->get('id');
        $data["result"] = $this->get_file($id);
        $this->template->load('template', 'process_view', $data);
    }

    private function get_mime_type($file)
    {
        $mime_types = array(
            "pdf" => "application/pdf"
        , "exe" => "application/octet-stream"
        , "zip" => "application/zip"
        , "docx" => "application/msword"
        , "doc" => "application/msword"
        , "xls" => "application/vnd.ms-excel"
        , "ppt" => "application/vnd.ms-powerpoint"
        , "gif" => "image/gif"
        , "png" => "image/png"
        , "jpeg" => "image/jpg"
        , "jpg" => "image/jpg"
        , "mp3" => "audio/mpeg"
        , "wav" => "audio/x-wav"
        , "mpeg" => "video/mpeg"
        , "mpg" => "video/mpeg"
        , "mpe" => "video/mpeg"
        , "mov" => "video/quicktime"
        , "avi" => "video/x-msvideo"
        , "3gp" => "video/3gpp"
        , "css" => "text/css"
        , "jsc" => "application/javascript"
        , "js" => "application/javascript"
        , "php" => "text/html"
        , "htm" => "text/html"
        , "html" => "text/html"
        );
        $extension = strtolower(end(explode('.', $file)));
        return $mime_types[$extension];
    }

    private function get_file($id)
    {
        $row = (array) $this->layout_model->detail_layout_article($id);
        if ($id <> "") {
            $file = $row['file'];
            $path = "./";
            $fullfile = $path . $file;
            $fullname = explode(".", $file);
            $ext = $fullname[count($fullname) - 1];
            $mime = $this->get_mime_type($ext);
            header("Content-Type: $mime");
            header('Content-Disposition: attachment; filename="' . basename($fullfile) . '"');
            header('Content-Length: ' . filesize($fullfile));
            readfile($fullfile);
            $this->session->set_flashdata('info', '<strong style="color: green;">Dowload Successfull</strong>');
        } else {
            $this->session->set_flashdata('info', '<strong style="color: red;">File not found</strong>');
        }
        redirect('/layout/process', 'refresh');
    }
}
