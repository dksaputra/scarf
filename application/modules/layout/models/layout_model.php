<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Layout_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function total_articles() {
        $a = "SELECT COUNT(article_id) as total FROM tb_articles";
        $b = $this->db->query($a);
        $b = $b->result_array();
        $c = $b[0]["total"];
        return $c;
    }

    public function listing_articles() {
        $perintah = "SELECT a.*,b.*,c.* FROM tb_layout_articles_approve a
			         LEFT JOIN tb_articles b ON b.article_id = a.article_id
			         LEFT JOIN tb_layout_articles c ON c.layout_article_id = a.layout_article_id";
        $hasil = $this->db->query($perintah);
        $hasil = $hasil->result_array();
        return $hasil;
    }

    public function listing_employees(){
        $perintah5 = "SELECT b.* FROM tb_employees b
	                  WHERE b.position = 'Reporter' or b.position = 'Redaktur'
	                  GROUP BY b.employee_id";
        $hasil5 = $this->db->query($perintah5);
        $hasil5 = $hasil5->result_array();
        return $hasil5;
    }

    public function detail_layout_article($id){
        $perintah = "SELECT * FROM tb_layout_articles WHERE layout_article_id = '$id'";
        $hasil = $this->db->query($perintah);
        $hasil = $hasil->row();
        return $hasil;
    }
}
