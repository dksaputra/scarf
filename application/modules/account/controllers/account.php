<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Account extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model("account_model");
    }

    public function change_password()
    {
        $data = array();
        $uid = $this->session->userdata('uid');
        $data['row'] = (array) $this->account_model->get_users($uid);
        $this->template->load('template', 'change_password_view', $data);
    }

    public function update_password()
    {
        $new_password = $this->input->post('password_baru');
        $uid = $this->session->userdata('uid');
        $password = md5($new_password);
        $hasil = $this->account_model->update_password($uid, $password);
        if ($hasil) {
            $this->session->set_flashdata('info', '<strong style="color: green;">Update Successful</strong>');
        } else {
            $this->session->set_flashdata('info', '<strong style="color: red;">Update Failed</strong>');
        }
        redirect('/home/', 'refresh');
    }

    public function login()
    {
        $data = array();
        $this->template->load('template', 'login_view', $data);
    }

    public function login_process()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $emma = md5($password);
        $row = (array) $this->account_model->get_login($username);
        if(count($row) > 0){
            if ($row['username'] == $username AND $row['password'] == $emma) {
                $uid = $row["username"];
                $pass = $row["password"];
                $nama = $row["name"];
                $type = $row["type_id"];
                $posisi = $row["position"];
                $this->session->set_userdata('posisi', $posisi);
                $this->session->set_userdata('uid', $uid);
                $this->session->set_userdata('pass', $pass);
                $this->session->set_userdata('nama', $nama);
                $this->session->set_userdata('type', $type);
                //pre($this->session->all_serdata());
                $this->session->set_flashdata('info', '<strong style="color: green;">Login Successful</strong>');
            } else {
                $this->session->set_flashdata('info', '<strong style="color: red;">Login Failed</strong>');
            }
        } else {
            $this->session->set_flashdata('info', '<strong style="color: red;">Login Failed</strong>');
        }
        redirect('/article/approval', 'refresh');
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('/home/', 'refresh');
    }
}

