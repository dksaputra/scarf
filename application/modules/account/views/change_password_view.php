<div id="content">
    <div class="content-detail">
        <p align="right">
            <strong>Login User : </strong>
            <?php echo $this->session->userdata('nama'); ?><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                Position: </strong><?php echo $this->session->userdata('posisi'); ?>
        </p>
        <form name=form1 method=post action="<?php echo site_url('account/update_password') ?>" onsubmit="return validate()">
            <input type="hidden" name="id" value="<?php echo $this->session->userdata('uid'); ?>">
            <fieldset>
                <legend>Change Password</legend>
                <table>
                    <tr>
                        <td width="150px">Name</td>
                        <td><input type=text name=name size=60 maxlength=100 value="<?php echo $row['name'] ?> " readonly="readonly"></td>
                    </tr>
                    <tr>
                        <td width="150px">Old Password</td>
                        <td><input type=text name=password size=60 maxlength=100 value="<?php echo $row['password'] ?> " readonly="readonly"></td>
                    </tr>
                    <tr>
                        <td width="150px">New Password</td>
                        <td><input type=text name=password_baru size=30 maxlength=39></td>
                    </tr>
                </table>
            </fieldset>
            <br>
            <input type=submit value=Save>
            <input type="button" value="Cancel" onClick="self.history.go(-1)"/>
        </form>
    </div>
</div>
