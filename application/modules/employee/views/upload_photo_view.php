<div id="content">
    <div class="content-detail">
        <p align="right">
            <strong>Login User : </strong>
            <?php echo $this->session->userdata('nama'); ?><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                Position: </strong><?php echo $this->session->userdata('posisi'); ?>
        </p>
        <form id="form1" name="form1" enctype="multipart/form-data" method="post" action="<?php echo site_url('employee/upload_photo_process') ?>">
            <fieldset>
                <legend>Upload Employee Photo</legend>
                <input type="hidden" name="id" value="<?php echo "$detail[employee_id]" ?>">
                <label>
                    <input name="foto" type="file" id="foto"/>
                </label>
                <label>
                    <input name="upload" type="submit" id="upload" value="Upload"/>
                    <input type="button" value="Back" onClick="self.history.go(-1)"/>
                </label>
            </fieldset>
        </form>
    </div>
</div>