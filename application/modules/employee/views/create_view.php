<?php echo js('select.js'); ?>
<?php echo js('employee_create.js'); ?>
<div id="content">
    <div class="content-detail">
        <p align="right">
            <strong>Login User : </strong>
            <?php echo $this->session->userdata('nama'); ?><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                Position: </strong><?php echo $this->session->userdata('posisi'); ?>
        </p>
        <form name=form1 method=post action="<?php echo site_url('employee/save') ?>" onsubmit="return validate()">
            <fieldset>
                <legend>Input Employee</legend>
                <table>
                    <tr>
                        <td width="150px">NIK</td>
                        <td><input type=text name=emp_id size=30 maxlength=12 onKeyUp=javascript:dodacheck_no(form1.emp_id)></td>
                    </tr>
                    <tr>
                        <td width="150px">Name</td>
                        <td><input type=text name=emp_name size=30 maxlength=39></td>
                    </tr>
                    <tr>
                        <td width="150px">Type User</td>
                        <td><select size=1 name=type_id>
                                <option selected="Select Type">Select Type</option>
                                <option>Admin</option>
                                <option>Reporter</option>
                                <option>Editor</option>
                                <option>Redaktur</option>
                                <option>Creative Designer</option>
                                <option>Editor-in Chief</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td width="150px">Sex</td>
                        <td><select size=1 name=emp_gender>
                                <option selected="Select Sex">Select Sex</option>
                                <option>Male</option>
                                <option>Female</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td width="150px">Birth Place</td>
                        <td><input type=text name=emp_birthplace size=30 maxlength=19></td>
                    </tr>
                    <tr>
                        <td width="150px">Birth Date</td>
                        <td>
                            <?php
                            echo "<SELECT NAME= tgl>";
                            echo "<OPTION VALUE=0 SELECTED >Date";
                            $tgl = '';
                            $bln = '';
                            $thn = '';
                            //Bentuk pilihan tanggal 0 sampai dengan 31
                            for ($i = 1; $i < 32; $i++) {
                                if ($tgl == $i)
                                    $sel = "SELECTED";
                                else
                                    $sel = "";

                                print("<OPTION VALUE=\"$i\" $sel>$i");
                            }
                            echo "</OPTION></SELECT>";
                            echo "<SELECT NAME=bln>";
                            echo "<OPTION VALUE=0 SELECTED >Month";
                            for ($i = 1; $i < 13; $i++) {
                                $sel[$i] = "";

                                if ($i == $bln)
                                    $sel[$i] = "SELECTED";
                            }
                            print("<OPTION VALUE=\"1\"  $sel[1]>Januari  ");
                            print("<OPTION VALUE=\"2\"  $sel[2]>Februari ");
                            print("<OPTION VALUE=\"3\"  $sel[3]>Maret    ");
                            print("<OPTION VALUE=\"4\"  $sel[4]>April    ");
                            print("<OPTION VALUE=\"5\"  $sel[5]>Mei      ");
                            print("<OPTION VALUE=\"6\"  $sel[6]>Juni     ");
                            print("<OPTION VALUE=\"7\"  $sel[7]>Juli     ");
                            print("<OPTION VALUE=\"8\"  $sel[8]>Agustus  ");
                            print("<OPTION VALUE=\"9\"  $sel[9]>September");
                            print("<OPTION VALUE=\"10\" $sel[10]>Oktober ");
                            print("<OPTION VALUE=\"11\" $sel[11]>November");
                            print("<OPTION VALUE=\"12\" $sel[12]>Desember");
                            echo "</OPTION></SELECT>";
                            echo "<SELECT NAME=thn>";
                            echo "<OPTION VALUE=0 SELECTED >Year";
                            // Bentuk pilihan tahun dari 1900
                            // sampai sekarang
                            $sekarang = (integer)Date("Y");
                            for ($i = 1980; $i <= $sekarang; $i++) {
                                if ($i == $thn)
                                    $sel = "SELECTED";
                                else
                                    $sel = "";

                                print("<OPTION VALUE=\"$i\" $sel>$i");
                            }
                            echo "</OPTION></SELECT>"; ?>
                        </td>
                    </tr>
                    <tr>
                        <td width="150px">Marital</td>
                        <td><select size=1 name=emp_marital>
                                <option selected="Select Marital">Select Marital</option>
                                <option>Single</option>
                                <option>Married</option>
                                <option>Divorce</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td width="150px">Religion</td>
                        <td><select size=1 name=emp_religion>
                                <option selected="Select Religion">Select Religion</option>
                                <option>Islam</option>
                                <option>Catholic Christians</option>
                                <option>Christian Protestant</option>
                                <option>Hindu</option>
                                <option>Buddha</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td width="150px">Education</td>
                        <td><select size=1 name=emp_lastedu>
                                <option selected="Select Education">Select Education</option>
                                <option>Elementary School</option>
                                <option>Junior High School</option>
                                <option>High School</option>
                                <option>D3</option>
                                <option>Bachelor</option>
                                <option>Magister</option>
                                <option>Doctor</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td width="150px">Position</td>
                        <td><select size=1 name=emp_position>
                                <option selected="Select Position">Select Position</option>
                                <option>Admin</option>
                                <option>Reporter</option>
                                <option>Editor</option>
                                <option>Redaktur</option>
                                <option>Creative Designer</option>
                                <option>Editor-in Chief</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td width="150px">Address</td>
                        <td><textarea name=emp_address rows="4" cols="50"></textarea></td>
                    </tr>
                    <tr>
                        <td width="150px">Phone</td>
                        <td><input type=text name=emp_phone size=30 maxlength=29
                                   onKeyUp=javascript:dodacheck_no(form1.emp_phone)></td>
                    </tr>
                    <tr>
                        <td width="150px">HP</td>
                        <td><input type=text name=emp_cellular size=30 maxlength=29
                                   onKeyUp=javascript:dodacheck_no(form1.emp_cellular)></td>
                    </tr>
                    <tr>
                        <td width="150px">Email</td>
                        <td><input type=text name=emp_email size=30 maxlength=19></td>
                    </tr>
                </table>
            </fieldset>
            <br>
            <br>
            <input type=submit value=Save>
            <input type=reset value=Clear>
            <input type="button" value="Back" onClick="self.history.go(-1)"/>
        </form>
    </div>
</div>