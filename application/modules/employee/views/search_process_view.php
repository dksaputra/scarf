<div id="content">
    <div class="content-detail">
        <p align="right">
            <strong>Login User : </strong>
            <?php echo $this->session->userdata('nama'); ?><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                Position: </strong><?php echo $this->session->userdata('posisi'); ?>
        </p>
        <font face=arial size=1>
            &nbsp|&nbsp;<a href="<?php echo site_url('employee/search') ?>">SEARCH AGAIN</a>&nbsp|&nbsp;
        </font>
        <br><br>
        <strong>Hasil pencarian :</strong>
        <br><br>
        <table class="TFtable">
            <tr>
                <td><b>NIK</b></td>
                <td><b>Name</b></td>
                <td><b>Tgl Lahir</b></td>
                <td><b>Jenis Kelamin</b></td>
                <td><b>Agama</b></td>
                <? if ($emp_id == $this->session->userdata('uid') or $this->session->userdata('type') == 1) { ?>
                    <td colspan="2" align="center"><b>Action</b></td>
                <? } ?>
            </tr>
            <?php
            $count = 0;
            $url_edit = site_url('employee/edit');
            $url_delete = site_url('employee/delete');
            foreach ($datas as $row) {
                $index = ($count + 1);
                if ($count % 2 == 1) {
                    $style = "row1";
                } else {
                    $style = "row2";
                }
                echo "<tr class='" . $style . "'>";
                echo "<td>$row[employee_id]</td>";
                echo "<td>$row[name]</td>";
                echo "<td>$row[birthdate]</td>";
                echo "<td>$row[gender]</td>";
                echo "<td>$row[religion]</td>";
                if ($row['employee_id'] == $this->session->userdata('uid') or $this->session->userdata('type') == 1) {
                    echo "<td><a href=\"$url_edit?id=$row[employee_id]\">Edit</a></td>";
                    echo "<td><a href=\"$url_delete?id=$row[employee_id]\">Delete</a></td></tr>";
                } else {
                    echo "<td></td>";
                    echo "<td></td>";
                }
                $count++;
            }
            ?>
            </tr>
        </table>
        <br>
    </div>
</div>
<tr>
    <td>&nbsp;&nbsp;Total : <b><?php echo $count; ?></b></td>
</tr>
