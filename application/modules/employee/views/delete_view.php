<div id="content">
    <div class="content-detail">
        <br>
        <form name="delete" method="post" action="<?php echo site_url('employee/delete_process') . '?id=' . $detail['employee_id']; ?>">
            <input type="hidden" name="id" value="<?php echo $detail['employee_id']; ?>">
            <table align="center" id="tbl_confirm" width="400" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="center" colspan="2" style="background-color:#dedede;"><b>Confimation to Deleting Data</b>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td align="left" width="40%" style="padding-left:10px"><b>NIK :</b></td>
                    <td align="left" width="60%">"<?php echo $detail['employee_id'] ?>"</td>
                </tr>
                <tr>
                    <td align="left" width="40%" style="padding-left:10px"><b>Name :</b></td>
                    <td align="left" width="60%">"<?php echo $detail['name'] ?>"</td>
                </tr>
                <tr>
                    <td align="left" width="40%" style="padding-left:10px"><b>Sex :</b></td>
                    <td align="left" width="60%">"<?php echo $detail['gender'] ?>"</td>
                </tr>
                <tr>
                    <td align="left" width="40%" style="padding-left:10px"><b>Birth Place :</b></td>
                    <td align="left" width="60%">"<?php echo $detail['birthplace'] ?>"</td>
                </tr>
                <tr>
                    <td align="left" width="40%" style="padding-left:10px"><b>Birth Date :</b></td>
                    <td align="left" width="60%">"<?php echo $detail['birthdate'] ?>"</td>
                </tr>
                <tr>
                    <td align="left" width="40%" style="padding-left:10px"><b>Religion :</b></td>
                    <td align="left" width="60%">"<?php echo $detail['religion'] ?>"</td>
                </tr>
                <tr>
                    <td align="left" width="40%" style="padding-left:10px"><b>Address :</b></td>
                    <td align="left" width="60%">"<?php echo $detail['address'] ?>"</td>
                </tr>
                <tr>
                    <td align="left" width="40%" style="padding-left:10px"><b>Phone :</b></td>
                    <td align="left" width="60%">"<?php echo $detail['phone'] ?>"</td>
                </tr>
                <tr>
                    <td align="left" width="40%" style="padding-left:10px"><b>Position :</b></td>
                    <td align="left" width="60%">"<?php echo $detail['position'] ?>"</td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align:center;"><br>Are you sure want to delete this data ?</td>
                <tr>
                    <td colspan="2" style="text-align:center;"><br>
                        <input type="submit" name="del" value="  Yes  "/>&nbsp;&nbsp;
                        <input type="button" value="Back" onClick="self.history.go(-1)"/></a></td>
                </tr>
                <tr>
                    <td colspan="2" class="no-border">&nbsp;</td>
                </tr>
            </table>
        </form>
    </div>
</div>
