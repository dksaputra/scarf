<?php echo js('select.js'); ?>
<?php echo js('employee_edit.js'); ?>
<div id="content">
    <div class="content-detail">
        <p align="right">
            <strong>Login User : </strong>
            <?php echo $this->session->userdata('nama'); ?><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                Position: </strong><?php echo $this->session->userdata('posisi'); ?>
        </p>
        <form name=form1 method=post action="<?php echo site_url('employee/update') ?>" onsubmit="return validate()">
            <input type="hidden" name="id" value="<?php echo "$detail[employee_id]" ?>">
            <fieldset>
                <legend>Edit Employee Data</legend>
                <table>
                    <tr>
                        <td width="150px">NIK</td>
                        <td><input type=text name=emp_id size=30 maxlength=12 value="<?php echo "$detail[employee_id]" ?>" onKeyUp=javascript:dodacheck_no(form1.emp_id)></td>
                    </tr>
                    <tr>
                        <td width="150px">Name</td>
                        <td><input type=text name=emp_name size=30 maxlength=39 value="<?php echo "$detail[name]" ?>">
                        </td>
                    </tr>
                    <tr>
                        <td width="150px">Position</td>
                        <td><select size=1 name=emp_position value="<?php echo "$detail[position]" ?>">
                                <option><?php echo "$detail[position]" ?></option>
                                <option>Reporter</option>
                                <option>Editor</option>
                                <option>Redaktur</option>
                                <option>Creative Designer</option>
                                <option>Editor-in Chief</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td width="150px">Sex</td>
                        <td><select size=1 name=emp_gender>
                                <option><?php echo "$detail[gender]" ?></option>
                                <option>Male</option>
                                <option>Female</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td width="150px">Birth Place</td>
                        <td><input type=text name=emp_birthplace size=30 maxlength=19
                                   value="<?php echo "$detail[birthplace]" ?>"></td>
                    </tr>
                    <tr>
                        <td width="150px">Birth Date</td>
                        <td><input type=text name=emp_birthdate size=30 maxlength=19
                                   value="<?php echo "$detail[birthdate]" ?>"></td>
                    </tr>
                    <tr>
                        <td width="150px">Marital</td>
                        <td><select size=1 name=emp_marital value="<?php echo "$detail[marital]" ?>">
                                <option><?php echo "$detail[marital]" ?></option>
                                <option>Single</option>
                                <option>Married</option>
                                <option>Divorce</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td width="150px">Religion</td>
                        <td><select size=1 name=emp_religion value="<?php echo "$detail[religion]" ?>">
                                <option><?php echo "$detail[religion]" ?></option>
                                <option>Islam</option>
                                <option>Catholic Christians</option>
                                <option>Christian Protestant</option>
                                <option>Hindu</option>
                                <option>Buddha</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td width="150px">Education</td>
                        <td><select size=1 name=emp_lastedu value="<?php echo "$detail[lastedu]" ?>">
                                <option><?php echo "$detail[lastedu]" ?></option>
                                <option>Elementary School</option>
                                <option>Junior High School</option>
                                <option>High School</option>
                                <option>D3</option>
                                <option>Bachelor</option>
                                <option>Magister</option>
                                <option>Doctor</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td width="150px">Address</td>
                        <td><textarea name=emp_address rows="4" cols="50"><?php echo "$detail[address]" ?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td width="150px">Phone</td>
                        <td><input type=text name=emp_phone size=30 maxlength=29 value="<?php echo "$detail[phone]" ?>"
                                   onKeyUp=javascript:dodacheck_no(form1.emp_phone)></td>
                    </tr>
                    <tr>
                        <td width="150px">HP</td>
                        <td><input type=text name=emp_cellular size=30 maxlength=29
                                   value="<?php echo "$detail[cellular]" ?>"
                                   onKeyUp=javascript:dodacheck_no(form1.emp_cellular)></td>
                    </tr>
                    <tr>
                        <td width="150px">Email</td>
                        <td><input type=text name=emp_email size=30 maxlength=19
                                   value="<?php echo "$detail[email]" ?>"></td>
                    </tr>
                </table>
            </fieldset>
            <br>
            <br>
            <input type=submit value=Save>
            <input type="button" value="Back" onClick="self.history.go(-1)"/>
        </form>
    </div>
</div>