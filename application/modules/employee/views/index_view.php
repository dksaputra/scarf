<div id="content">
    <p align="right">
        <strong>Login User : </strong>
        <?php echo $this->session->userdata('nama'); ?><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            Position: </strong><?php echo $this->session->userdata('posisi'); ?>
    </p>
    <form action="<?php echo site_url('employee/search_process') ?>" method=post>
        <fieldset>
            <legend>Search Employee</legend>
            NIK <input type=text name=employee_id>&nbsp;&nbsp;
            Name <input type=text name=emp_name>&nbsp;&nbsp;
            <input type=submit value=Search>
        </fieldset>
    </form>
    <br><br>
    <strong>Employee List :</strong>
    <br><br>
    <table class="TFtable">
        <tr>
            <td><b>Name</b></td>
            <td><b>NIK</b></td>
            <td><b>Sex</b></td>
            <td><b>Birth Date</b></td>
            <td><b>Position</b></td>
            <td><b>Photo</b></td>
            <td colspan="3" align="center"><b>Action</b></td>
        </tr>
        <?php
        $base_url = base_url();
        $url_photo = site_url('employee/upload_photo');
        $url_detail = site_url('employee/detail');
        $url_edit = site_url('employee/edit');
        $url_delete = site_url('employee/delete');
        foreach ($datas as $row) {
            $index = ($count + 1);
            if ($count % 2 == 1) {
                $style = "row1";
            } else {
                $style = "row2";
            }
            echo "<tr class='" . $style . "'>";
            echo "<td>$row[name]</td>";
            echo "<td>$row[employee_id]</td>";
            echo "<td>$row[gender]</td>";
            echo "<td>$row[birthdate]</td>";
            echo "<td>$row[position]</td>";
            echo "<td><img width=50 height=40 src='$base_url$row[photo]' /></td>";
            echo "<td align='center'><a href=\"$url_photo?id=$row[employee_id]\">Upload Foto</a></td>";
            //echo "<td align='center'><a href=\"$url_detail?id=$row[employee_id]\">Detail</a></td>";
            echo "<td align='center'><a href=\"$url_edit?id=$row[employee_id]\">Edit</a></td>";
            if ($this->session->userdata('type') == 1) {
                echo "<td align='center'><a href=\"$url_delete?id=$row[employee_id]\">Delete</a></td></tr>";
            }
            $count++;
        }
        ?>
    </table>
</div>
<br>
<tr class="thstyle">
    <td class="thbottom" colspan="8">
        <?php
        if ($hal > 1) {
            echo "<a href='?page=" . ($hal - 1) . "&count=" . (($hal * 5) - 30) . "'><b>&lt;&lt;Previous</b></a>";
        } else {
            echo "&lt;&lt;Previous";
        }

        if ($hal < $d) {
            echo "&nbsp;&nbsp;&nbsp;<a href='?page=" . ($hal + 1) . "&count=" . ($hal * 30) . "'><b>Next&gt;&gt;</b></a>";
        } else {
            echo "&nbsp;&nbsp;&nbsp;Next&gt;&gt;";
        }
        ?></td>
</tr>
<tr>
    <td colspan="8" class="thbtm_hasil">Total Employee : <b><?php echo $c; ?></b></td>
</tr>
</table>
<span style="float:left;padding-left:0px;"><b><?php echo "Page:&nbsp;" . $hal; ?></b></span><br><br>