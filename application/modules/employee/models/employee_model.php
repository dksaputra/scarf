<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Employee_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function delete($id) {
        $perintah = "DELETE FROM tb_employees WHERE employee_id='$id'";
        $hasil = $this->db->query($perintah);
        return $hasil;
    }

    public function get_employee_by_id($id) {
        $perintah = "SELECT * FROM tb_employees WHERE employee_id='$id'";
        $hasil = $this->db->query($perintah);
        $hasil = $hasil->row();
        return $hasil;
    }

    public function total_articles() {
        $a = "SELECT COUNT(article_id) as total FROM tb_articles";
        $b = $this->db->query($a);
        $b = $b->result_array();
        $c = $b[0]["total"];
        return $c;
    }

    public function get_employees($limit){
        $perintah = "SELECT * FROM tb_employees ORDER BY employee_id ASC LIMIT " . $limit . ",30";
        $hasil = $this->db->query($perintah);
        $hasil = $hasil->result_array();
        return $hasil;
    }

    public function get_employee($uid){
        $perintah = "SELECT * FROM tb_employees where employee_id = '$uid'";
        $hasil = $this->db->query($perintah);
        $hasil = $hasil->result_array();
        return $hasil;
    }

    public function update_photo($id, $fileName){
        $perintah = "UPDATE tb_employees SET photo='uploads/$fileName' WHERE employee_id=$id";
        $hasil = $this->db->query($perintah);
        //pre($this->db->last_query());
        return $hasil;
    }

    public function update_employee($employee_id, $emp_name, $emp_gender, $emp_birthplace, $emp_birthdate, $emp_religion, $emp_lastedu, $emp_address, $emp_phone, $emp_cellular, $emp_email, $emp_marital, $emp_position){
        $perintah = "UPDATE tb_employees SET
                      name='$emp_name'
                      ,gender='$emp_gender'
                      ,birthplace='$emp_birthplace'
                      ,birthdate='$emp_birthdate'
                      ,religion='$emp_religion'
                      ,lastedu='$emp_lastedu'
                      ,position='$emp_position'
                      ,address='$emp_address'
                      ,phone='$emp_phone'
                      ,cellular='$emp_cellular'
                      ,email='$emp_email'
                      ,marital='$emp_marital'
                      WHERE employee_id=$employee_id";
        $hasil = $this->db->query($perintah);
        return $hasil;
    }

    public function insert_employee($employee_id, $emp_name, $emp_gender, $emp_birthplace, $emp_birthdate, $emp_religion, $emp_lastedu, $emp_address, $emp_phone, $emp_cellular, $emp_email, $emp_marital, $emp_position, $password, $type){
        $perintah1 = "INSERT INTO tb_employees (
                                          employee_id
                                          , name
                                          , birthplace
                                          , birthdate
                                          , gender
                                          , religion
                                          , lastedu
                                          , position
                                          , address
                                          , phone
                                          , cellular
                                          , email
                                          , marital
                                          , photo
                                          , status
                                          , updated_date
                                        )
                                VALUES (
                                          '$employee_id'
                                          , '$emp_name'
                                          , '$emp_birthplace'
                                          , '$emp_birthdate'
                                          , '$emp_gender'
                                          , '$emp_religion'
                                          , '$emp_lastedu'
                                          , '$emp_position'
                                          , '$emp_address'
                                          , '$emp_phone'
                                          , '$emp_cellular'
                                          , '$emp_email'
                                          , '$emp_marital'
                                          , 0
                                          , 1
                                          , now()
                                      )";
        $perintah2 = "INSERT INTO tb_users (username, password, name, type_id) VALUES ('$employee_id', '$password', '$emp_name', '$type')";
        $result1 = $this->db->query($perintah1);
        $result2 = $this->db->query($perintah2);
        //pre($this->db->last_query());
        return $result1;
    }

    public function search_employee($emp_id, $emp_name){
        $perintah = "SELECT * FROM tb_employees WHERE employee_id LIKE '%$emp_id%' AND name LIKE '%$emp_name%' ORDER BY employee_id ASC";
        $hasil = $this->db->query($perintah);
        $hasil = $hasil->result_array();
        return $hasil;
    }
}
