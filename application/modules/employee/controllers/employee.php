<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Employee extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        if($this->session->userdata('uid') == ""){
            redirect('/account/login', 'refresh');
        }
        $this->load->model("employee_model");
    }

    public function index()
    {
        $count = 0;
        $hal = $this->input->get('page');
        $jlh = $this->input->get('count');

        if ($jlh <= 0) {
            $count = 0;
        } else {
            $count = $jlh;
        }

        $c = $this->employee_model->total_articles();
        $d = floor(($c - 1) / 30) + 1;

        if ($hal <= 0) {
            $hal = 1;
        }
        if ($hal > $d) {
            $hal = $d;
        }

        $batas = (30 * ($hal - 1));
        if ($batas < 0) {
            $batas2 = 0;
        } else {
            $batas2 = $batas;
        }

        if ($this->session->userdata('type') == 1) {
            $hasil = $this->employee_model->get_employees($batas2);
        } else if ($this->session->userdata('type') > 1) {
            $hasil = $this->employee_model->get_employee($this->session->userdata('uid'));
        } else {
            $hasil = $this->employee_model->get_employees($batas2);
        }

        $data = array();
        $data['hal'] = $hal;
        $data['c'] = $c;
        $data['d'] = $d;
        $data['count'] = $count;
        $data['datas'] = $hasil;
        $this->template->load('template', 'index_view', $data);
    }

    public function search()
    {
        $data = array();
        $this->template->load('template', 'search_view', $data);
    }

    public function search_process()
    {
        $data = array();
        $emp_id = $this->input->post('emp_id');
        $emp_name = $this->input->post('emp_name');
        $data['emp_id'] = $emp_id;
        $data['emp_name'] = $emp_name;
        $data['datas'] = $this->employee_model->search_employee($emp_id, $emp_name);
        $this->template->load('template', 'search_process_view', $data);
    }

    public function create()
    {
        $data = array();
        $this->template->load('template', 'create_view', $data);
    }

    public function save()
    {
        $emp_id = $this->input->post('emp_id');
        $emp_name = $this->input->post('emp_name');
        $type = $this->input->post('type_id');
        $type_id = "";
        if ($type == "Admin") {
            $type_id = 1;
        }
        if ($type == "Reporter") {
            $type_id = 2;
        }
        if ($type == "Editor") {
            $type_id = 3;
        }
        if ($type == "Redaktur") {
            $type_id = 4;
        }
        if ($type == "Creative Designer") {
            $type_id = 5;
        }
        if ($type == "Editor-in Chief") {
            $type_id = 6;
        }
        $password = md5($emp_id);
        $emp_gender = $this->input->post('emp_gender');
        $emp_birthplace = $this->input->post('emp_birthplace');
        $tgl = $this->input->post('tgl');
        $bln = $this->input->post('bln');
        $thn = $this->input->post('thn');
        $emp_birthdate = $this->input->post('emp_birthdate');
        $emp_birthdate = substr_replace($emp_birthdate, $thn, 0, 0);
        $emp_birthdate = substr_replace($emp_birthdate, "-", 0, 0);
        $emp_birthdate = substr_replace($emp_birthdate, $bln, 0, 0);
        $emp_birthdate = substr_replace($emp_birthdate, "-", 0, 0);
        $emp_birthdate = substr_replace($emp_birthdate, $tgl, 0, 0);
        $emp_religion = $this->input->post('emp_religion');
        $emp_lastedu = $this->input->post('emp_lastedu');
        $emp_position = $this->input->post('emp_position');
        $emp_address = $this->input->post('emp_address');
        $emp_phone = $this->input->post('emp_phone');
        $emp_cellular = $this->input->post('emp_cellular');
        $emp_marital = $this->input->post('emp_marital');
        $emp_email = $this->input->post('emp_email');
        $result = $this->employee_model->insert_employee($emp_id, $emp_name, $emp_gender, $emp_birthplace, $emp_birthdate, $emp_religion, $emp_lastedu, $emp_address, $emp_phone, $emp_cellular, $emp_email, $emp_marital, $emp_position, $password, $type_id);
        //pre($this->db->last_query());
        if (!empty($emp_id) AND !empty($emp_name) AND !empty($emp_gender)) {
            if ($result) {
                $this->session->set_flashdata('info', '<strong style="color: green;">Save Successfull</strong>');
            }
        } else {
            $this->session->set_flashdata('info', '<strong style="color: red;">Save Failed</strong>');
        }
        redirect('/employee/index/', 'refresh');
    }

    public function edit()
    {
        $data = array();
        $id = $this->input->get('id');
        $data["detail"] = (array) $this->employee_model->get_employee_by_id($id);
        $this->template->load('template', 'edit_view', $data);
    }

    public function update()
    {
        $emp_id = $this->input->post('emp_id');
        $emp_name = $this->input->post('emp_name');
        $emp_gender = $this->input->post('emp_gender');
        $emp_birthplace = $this->input->post('emp_birthplace');
        $emp_birthdate = $this->input->post('emp_birthdate');
        $emp_religion = $this->input->post('emp_religion');
        $emp_lastedu = $this->input->post('emp_lastedu');
        $emp_position = $this->input->post('emp_position');
        $emp_address = $this->input->post('emp_address');
        $emp_phone = $this->input->post('emp_phone');
        $emp_cellular = $this->input->post('emp_cellular');
        $emp_marital = $this->input->post('emp_marital');
        $emp_email = $this->input->post('emp_email');
        $hasil = $this->employee_model->update_employee($emp_id, $emp_name, $emp_gender, $emp_birthplace, $emp_birthdate, $emp_religion, $emp_lastedu, $emp_address, $emp_phone, $emp_cellular, $emp_email, $emp_marital, $emp_position);
        if ($hasil) {
            $this->session->set_flashdata('info', '<strong style="color: green;">Update Successfull</strong>');
        } else {
            $this->session->set_flashdata('info', '<strong style="color: red;">Update Failed</strong>');
        }
        redirect('/employee/edit?id=' . $emp_id, 'refresh');
    }

    public function delete()
    {
        $data = array();
        $id = $this->input->get('id');
        $data["detail"] = (array) $this->employee_model->get_employee_by_id($id);
        $this->template->load('template', 'delete_view', $data);
    }

    public function delete_process()
    {
        $id = $this->input->post('id');
        $result = $this->employee_model->delete($id);
        if($result){
            $this->session->set_flashdata('info', '<strong style="color: green;">Delete Successfull</strong>');
        }else{
            $this->session->set_flashdata('info', '<strong style="color: red;">Delete Failed</strong>');
        }
        redirect('/employee/index', 'refresh');
    }

    public function upload_photo()
    {
        $data = array();
        $id = $this->input->get('id');
        $data["detail"] = (array) $this->employee_model->get_employee_by_id($id);
        $this->template->load('template', 'upload_photo_view', $data);
    }

    public function upload_photo_process()
    {
        $id = $this->input->post('id');
        $filename = time();
        $fileext = pathinfo($_FILES['foto']['name'], PATHINFO_EXTENSION);
        $filesize = $_FILES['foto']['size'];
        $fileerror = $_FILES['foto']['error'];
        $uploaddir = './uploads/';
        $lokasi = $uploaddir . $filename . '.' . $fileext;
        if ($filesize > 0 || $fileerror == 0) {
            $move = move_uploaded_file($_FILES['foto']['tmp_name'], $lokasi); //save gambar ke folder
            if ($move) {
                $this->employee_model->update_photo($id, $filename . '.' . $fileext);
                $this->session->set_flashdata('info', '<strong style="color: green;">Upload Successfull</strong>');
            } else {
                $this->session->set_flashdata('info', '<strong style="color: red;">Upload Failed</strong>');
            }
        } else {
            $this->session->set_flashdata('info', '<strong style="color: red;">Upload Error</strong>');
        }
        redirect('/employee/upload_photo?id=' . $id, 'refresh');
    }
}
