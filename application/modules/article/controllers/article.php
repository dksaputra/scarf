<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Article extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('uid') == "") {
            redirect('/account/login', 'refresh');
        }
        $this->load->model("article_model");
    }

    public function index()
    {
        $count = 0;
        $hal = $this->input->get('page');
        $jlh = $this->input->get('count');

        if ($jlh <= 0) {
            $count = 0;
        } else {
            $count = $jlh;
        }

        $c = $this->article_model->total_articles();
        $d = floor(($c - 1) / 30) + 1;

        if ($hal <= 0) {
            $hal = 1;
        }
        if ($hal > $d) {
            $hal = $d;
        }

        $batas = (30 * ($hal - 1));
        if ($batas < 0) {
            $batas2 = 0;
        } else {
            $batas2 = $batas;
        }

        //$hasil = $this->article_model->listing_articles($batas2);
        $get_sort = $this->input->get('sort');
        if ($get_sort == 'asc') {
            $sort = " ORDER BY a.article_date ASC";
        } else {
            $sort = " ORDER BY a.article_date DESC";
        }

        $data = array();
        $data['hal'] = $hal;
        $data['c'] = $c;
        $data['d'] = $d;
        $data['count'] = $count;
        $data['data_employees'] = $this->article_model->get_employees();
        $data['data_articles'] = $this->article_model->listing_articles($sort, $batas2);
        $this->template->load('template', 'index_view', $data);
    }

    public function myarticle()
    {
        $count = 0;
        $hal = $this->input->get('page');
        $jlh = $this->input->get('count');

        if ($jlh <= 0) {
            $count = 0;
        } else {
            $count = $jlh;
        }

        $c = $this->article_model->total_articles();
        $d = floor(($c - 1) / 30) + 1;

        if ($hal <= 0) {
            $hal = 1;
        }
        if ($hal > $d) {
            $hal = $d;
        }
        //
        $batas = (30 * ($hal - 1));
        if ($batas < 0) {
            $batas2 = 0;
        } else {
            $batas2 = $batas;
        }

        //$hasil = $this->article_model->listing_articles($batas2);
        $get_sort = $this->input->get('sort');
        if ($get_sort == 'asc') {
            $sort = " ORDER BY a.article_date ASC";
        } else {
            $sort = " ORDER BY a.article_date DESC";
        }

        $get_sort = $this->input->get('sort');
        if ($get_sort == 'asc') {
            $sort = " ORDER BY a.article_date ASC";
        } else {
            $sort = " ORDER BY a.article_date DESC";
        }

        $data = array();
        $data['hal'] = $hal;
        $data['c'] = $c;
        $data['d'] = $d;
        $data['count'] = $count;
        $data['data_employees'] = $this->article_model->get_employees();
        $data['data_articles'] = $this->article_model->listing_my_articles($this->session->userdata('type'), $sort, $batas2);
        $this->template->load('template', 'my_article_view', $data);
    }

    public function approval()
    {
        $count = 0;
        $hal = $this->input->get('page');
        $jlh = $this->input->get('count');

        if ($jlh <= 0) {
            $count = 0;
        } else {
            $count = $jlh;
        }

        $c = $this->article_model->total_articles();
        $d = floor(($c - 1) / 30) + 1;

        if ($hal <= 0) {
            $hal = 1;
        }
        if ($hal > $d) {
            $hal = $d;
        }
        //
        $batas = (30 * ($hal - 1));
        if ($batas < 0) {
            $batas2 = 0;
        } else {
            $batas2 = $batas;
        }

        //$hasil = $this->article_model->listing_articles($batas2);
        $get_sort = $this->input->get('sort');
        if ($get_sort == 'asc') {
            $sort = " ORDER BY a.article_date ASC";
        } else {
            $sort = " ORDER BY a.article_date DESC";
        }

        $get_sort = $this->input->get('sort');
        if ($get_sort == 'asc') {
            $sort = " ORDER BY a.article_date ASC";
        } else {
            $sort = " ORDER BY a.article_date DESC";
        }

        $data = array();
        $data['hal'] = $hal;
        $data['c'] = $c;
        $data['d'] = $d;
        $data['count'] = $count;
        $data['data_employees'] = $this->article_model->get_employees();
        $data['data_articles'] = $this->article_model->listing_articles_approval($this->session->userdata('type'), $sort, $batas2);
        $this->template->load('template', 'approval_view', $data);
    }

    public function detail()
    {
        $id = $this->input->get('id');
        $data = array();
        $data['id'] = $id;
        $data['detail'] = (array)$this->article_model->get_article_by_id($id);
        $data['photo'] = (array)$this->article_model->get_photo_article($id);
        $data['total_layouts'] = $this->article_model->total_layouts_by_id($id);
        $this->template->load('template', 'detail_view', $data);
    }

    public function search()
    {
        $data = array();
        $data['data_employees'] = $this->article_model->get_employees();
        $this->template->load('template', 'search_view', $data);
    }

    public function search_process()
    {
        $tgl = $this->input->post('tgl');
        $bln = $this->input->post('bln');
        $thn = $this->input->post('thn');
        $article_date = '';
        $article_date = substr_replace($article_date, $tgl, 0, 0);
        $article_date = substr_replace($article_date, "-", 0, 0);
        $article_date = substr_replace($article_date, $bln, 0, 0);
        $article_date = substr_replace($article_date, "-", 0, 0);
        $article_date = substr_replace($article_date, $thn, 0, 0);
        $user_id = $this->input->post('user_id');
        $title = $this->input->post('title');
        $data = array();
        $data['data_articles'] = $this->article_model->search($article_date, $user_id, $title);
        $this->template->load('template', 'search_process_view', $data);
    }

    public function create()
    {
        $data = array();
        $get_editor = $this->article_model->get_editor();
        $data['hasil6'] = $get_editor['hasil6'];
        $data['hasil7'] = $get_editor['hasil7'];
        $data['hasil8'] = $get_editor['hasil8'];
        $data['hasil9'] = $get_editor['hasil9'];
        $this->template->load('template', 'create_view', $data);
    }

    public function save()
    {
        $title = $this->input->post('title');
        $content = $this->input->post('content');
        $approve_editor = $this->input->post('approve_editor');
        $status = "";
        if ($this->session->userdata('type') == 1) {
            $status = 0;
        }//admin
        if ($this->session->userdata('type') == 2) {
            $status = 0;
        }//reporter
        if ($this->session->userdata('type') == 3) {
            $status = 1;
        }//editor
        if ($this->session->userdata('type') == 4) {
            $status = 2;
        }//redaktur
        if ($this->session->userdata('type') == 5) {
            $status = 3;
        }//chief
        $tgl = $this->input->post('tgl');
        $bln = $this->input->post('bln');
        $thn = $this->input->post('thn');
        $article_date = "";
        $article_date = substr_replace($article_date, $tgl, 0, 0);
        $article_date = substr_replace($article_date, "-", 0, 0);
        $article_date = substr_replace($article_date, $bln, 0, 0);
        $article_date = substr_replace($article_date, "-", 0, 0);
        $article_date = substr_replace($article_date, $thn, 0, 0);
        $article_id = $this->article_model->save_article($article_date, $title, $content, $status, $approve_editor);
        $data = array();
        if ($this->input->post('submit') && $article_id > 0) {
            /*$j = 0; //Variable for indexing uploaded image
            $target_path = "./uploads/"; //Declaring Path for uploaded images
            for ($i = 0; $i < count($_FILES['foto']['name']); $i++) {//loop to get individual element from the array
                $validextensions = array("jpeg", "jpg", "png");  //Extensions which are allowed
                $ext = explode('.', basename($_FILES['foto']['name'][$i]));//explode file name from dot(.)
                $file_extension = end($ext); //store extensions in the variable

                $target_path = $target_path . time() . "." . $ext[count($ext) - 1];//set the target path with a new name of image
                $j = $j + 1;//increment the number of uploaded images according to the files in array
                $move = move_uploaded_file($_FILES['foto']['tmp_name'][$i], $target_path); //save gambar ke folder

                if (in_array($file_extension, $validextensions)) {
                    if ($move) {//if file moved to uploads folder
                        $this->article_model->save_photo_article($article_id, 'uploads/' . time() . "." . $ext[count($ext) - 1]);
                        $this->session->set_flashdata('info', '<strong style="color: green;">Save Successful</strong>');
                    } else {//if file was not moved.
                        $this->session->set_flashdata('info', '<strong style="color: red;">please try again!.</strong>');
                    }
                } else {//if file size and file type was incorrect.
                    $this->session->set_flashdata('info', '<strong style="color: red;">).***Invalid file Size or Type***</strong>');
                }
            }*/
            $filename = time();
            $fileext = pathinfo($_FILES['foto']['name'], PATHINFO_EXTENSION);
            $filesize = $_FILES['foto']['size'];
            $fileerror = $_FILES['foto']['error'];
            $uploaddir = './uploads/';
            $lokasi = $uploaddir . $filename . '.' . $fileext;
            if ($filesize > 0 || $fileerror == 0) {
                $move = move_uploaded_file($_FILES['foto']['tmp_name'], $lokasi); //save gambar ke folder
                if ($move) {
                    $this->article_model->save_photo_article($article_id, 'uploads/' . $filename . '.' . $fileext);
                    $this->session->set_flashdata('info', '<strong style="color: green;">Upload Successfull</strong>');
                } else {
                    $this->session->set_flashdata('info', '<strong style="color: red;">Upload Failed</strong>');
                }
            } else {
                $this->session->set_flashdata('info', '<strong style="color: red;">Upload Error</strong>');
            }
        } else {
            $this->session->set_flashdata('info', '<strong style="color: red;">please try again!.</strong>');
        }
        redirect('/article/approval', 'refresh');
    }

    public function edit()
    {
        $data = array();
        $id = $this->input->get('id');
        $data['detail'] = (array)$this->article_model->get_article_by_id($id);
        $data['editor'] = (array)$this->article_model->get_position_editor();
        $this->template->load('template', 'edit_view', $data);
    }

    public function update()
    {
        $article_id = $this->input->post('id');
        $article_date = $this->input->post('article_date');
        $title = $this->input->post('title');
        $content = $this->input->post('content');
        $approve_editor = $this->input->post('approve_editor');
        $article = (array)$this->article_model->get_article_by_id($article_id);

        /*foreach ($article as $row) {
            $existing_file = $row[file];
        }
        $layout = (empty($_FILES['layout']['name']) ? $existing_file : $_FILES['layout']['name']);
        if ($_FILES['gambar']['size'] > 1000000) {
            $this->session->set_flashdata('info', '<strong style="color: red;">File Terlalu Besar</strong>');
            redirect('/article/index', 'refresh');
        }
        $uploaddir = './uploads/layout/';
        $lokasi = $uploaddir . $layout;
        move_uploaded_file($_FILES['gambar']['tmp_name'], $lokasi);
        move_uploaded_file($_FILES['layout']['tmp_name'], $lokasi);*/

        $hasil = $this->article_model->update_article($article_id, $article_date, $title, $content, $approve_editor);
        if ($hasil) {
            $this->session->set_flashdata('info', '<strong style="color: green;">Update Successful</strong>');
        } else {
            $this->session->set_flashdata('info', '<strong style="color: red;">Update Failed</strong>');
        }
        redirect('/article/approval', 'refresh');
    }

    public function delete()
    {
        $data = array();
        $id = $this->input->get('id');
        $data["detail"] = (array)$this->article_model->delete_article_detail($id);
        $this->template->load('template', 'delete_article_view', $data);
    }

    public function delete_photo()
    {
        $data = array();
        $id = $this->input->get('id');
        $data["detail"] = (array)$this->article_model->delete_photo_detail($id);
        $this->template->load('template', 'delete_photo_view', $data);
    }

    public function delete_process()
    {
        $id = $this->input->post('id');
        $result = $this->article_model->delete($id);
        if ($result) {
            $this->session->set_flashdata('info', '<strong style="color: green;">Delete Successful</strong>');
        } else {
            $this->session->set_flashdata('info', '<strong style="color: red;">Save Failed</strong>');
        }
        redirect('/article/approval', 'refresh');
    }

    public function delete_photo_process()
    {
        $id = $this->input->post('id');
        $result = $this->article_model->delete_photo($id);
        if ($result) {
            $this->session->set_flashdata('info', '<strong style="color: green;">Delete Successful</strong>');
        } else {
            $this->session->set_flashdata('info', '<strong style="color: red;">Delete Failed</strong>');
        }
        redirect('/article/approval', 'refresh');
    }

    public function decline_process()
    {
        $article_id = $this->input->post('id');
        $note = $this->input->post('note');
        $status = $this->input->post('status');
        $result = $this->article_model->get_decline($article_id);
        $result = $this->article_model->update_status_article($article_id, $status, $note);
        if ($result) {
            $this->session->set_flashdata('info', '<strong style="color: green;">Decline Successful</strong>');
        } else {
            $this->session->set_flashdata('info', '<strong style="color: red;">Decline Failed</strong>');
        }
        redirect('/article/approval', 'refresh');
    }

    public function upload_layout()
    {
        $data = array();
        $id = $this->input->get('id');
        $get_layout = $this->article_model->get_layout($id);
        $data['aksi3'] = $get_layout['aksi3'];
        $data['aksi4'] = $get_layout['aksi4'];
        $data['hasil'] = $get_layout['hasil'];
        $data['hasil6'] = $get_layout['hasil6'];
        $data['detail'] = (array)$this->article_model->get_article_by_id($id);
        $this->template->load('template', 'upload_layout_view', $data);
    }

    public function select_layout_process()
    {
        $id = $this->input->post('id');
        $layout_article_id = $this->input->post('layout_article_id');
        $result = $this->article_model->save_layout_article_approve($id, $layout_article_id);
        //pre($this->db->last_query());
        if ($result) {
            $this->session->set_flashdata('info', '<strong style="color: green;">Save Successful</strong>');
        } else {
            $this->session->set_flashdata('info', '<strong style="color: red;">Save Failed</strong>');
        }
        redirect('/article/approval', 'refresh');
    }

    public function upload_layout_process()
    {
        $id = $this->input->post('id');
        $filename = time();
        $fileext = pathinfo($_FILES['layout']['name'], PATHINFO_EXTENSION);
        $filesize = $_FILES['layout']['size'];
        $fileerror = $_FILES['layout']['error'];
        $uploaddir = './uploads/';
        $lokasi = $uploaddir . $filename . '.' . $fileext;
        if ($filesize > 0 || $fileerror == 0) {
            $move = move_uploaded_file($_FILES['layout']['tmp_name'], $lokasi); //save gambar ke folder
            if ($move) {
                $this->article_model->save_layout_article($id, 'uploads/' . $filename . '.' . $fileext);
                $this->session->set_flashdata('info', '<strong style="color: green;">Upload Successfull</strong>');
            } else {
                $this->session->set_flashdata('info', '<strong style="color: red;">Upload Failed</strong>');
            }
        } else {
            $this->session->set_flashdata('info', '<strong style="color: red;">Upload Error</strong>');
        }
        redirect('/article/upload_layout?id=' . $id, 'refresh');
    }

    public function upload_photo()
    {
        $data = array();
        $id = $this->input->get('id');
        $data['detail'] = (array)$this->article_model->get_article_by_id($id);
        $this->template->load('template', 'upload_photo_view', $data);
    }

    public function upload_photo_process()
    {
        $id = $this->input->post('id');
        $filename = time();
        $fileext = pathinfo($_FILES['foto']['name'], PATHINFO_EXTENSION);
        $filesize = $_FILES['foto']['size'];
        $fileerror = $_FILES['foto']['error'];
        $uploaddir = './uploads/';
        $lokasi = $uploaddir . $filename . '.' . $fileext;
        if ($filesize > 0 || $fileerror == 0) {
            $move = move_uploaded_file($_FILES['foto']['tmp_name'], $lokasi); //save gambar ke folder
            if ($move) {
                $this->article_model->save_photo_article($id, 'uploads/' . $filename . '.' . $fileext);
                $this->session->set_flashdata('info', '<strong style="color: green;">Upload Successfull</strong>');
            } else {
                $this->session->set_flashdata('info', '<strong style="color: red;">Upload Failed</strong>');
            }
        } else {
            $this->session->set_flashdata('info', '<strong style="color: red;">Upload Error</strong>');
        }
        redirect('/article/upload_photo?id=' . $id, 'refresh');
    }

    public function send_email_process()
    {
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => 'xxx@gmail.com', // change it to yours
            'smtp_pass' => 'xxx', // change it to yours
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );

        $id = $this->input->post('id');
        $type_id = $this->input->post('type');
        $status = $this->input->post('status');
        $email_template = $this->article_model->get_email_template($type_id);
        $detail = (array)$this->article_model->get_article_by_id($id);

        if (isset($detail["article_id"]) AND $detail["article_id"] <> "") {
            /*$content = str_replace("[redaktur]", "Bpk/Ibu Creative Designer", $email_template["content"]);
            $content = str_replace("[tgl]", $detail["article_date"], $content);
            $content = str_replace("[judul]", $detail["title"], $content);

            $this->load->library('email', $config);
            $this->email->set_newline("\r\n");
            $this->email->from('ak.fandi@gmail.com'); // change it to yours
            $this->email->to('xxx@gmail.com');// change it to yours
            $this->email->subject($email_template["subject"]);
            $this->email->message($content);*/

            $this->article_model->update_status_article_email($id, $status);
            $this->session->set_flashdata('info', '<strong style="color: green;">Email Sent</strong>');
           // pre($this->db->last_query());
            /*if ($this->email->send()) {
                $this->article_model->update_status_article_email($id, $status);
                $this->session->set_flashdata('info', '<strong style="color: green;">Email Sent</strong>');
            } else {
                $this->session->set_flashdata('info', '<strong style="color: red;">'.$this->email->print_debugger().'</strong>');
            }*/
        } else {
            $this->session->set_flashdata('info', '<strong style="color: red;">Email Send Error</strong>');
        }

        redirect('/article/approval', 'refresh');
    }
}
