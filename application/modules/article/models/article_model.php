<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Article_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function total_articles()
    {
        $a = "SELECT COUNT(article_id) AS total FROM tb_articles";
        $b = $this->db->query($a);
        $b = $b->result_array();
        $c = $b[0]["total"];
        return $c;
    }

    public function listing_articles($sort, $limit)
    {
        $perintah = "SELECT a.*,b.*,c.position as approval_position, a.status AS article_status FROM tb_articles a
			left join tb_employees b on b.employee_id = a.employee_id
			left join tb_employees c on c.employee_id = a.approve_by
			$sort LIMIT " . $limit . ",30";
        $hasil = $this->db->query($perintah);
        $hasil = $hasil->result_array();
        return $hasil;
    }

    public function listing_my_articles($type, $sort, $limit)
    {
        $uid = $this->session->userdata('uid');
        if ($type == 1) {
            $perintah = "SELECT a.*,b.*,c.position as approval_position, a.status AS article_status FROM tb_articles a
			left join tb_employees b on b.employee_id = a.employee_id
			left join tb_employees c on c.employee_id = a.approve_by
			$sort LIMIT " . $limit . ",30";
        } else if ($type == 2)//reporter
        {
            $perintah = "SELECT a.*,b.*,c.position as approval_position, a.status AS article_status FROM tb_articles a
			left join tb_employees b on b.employee_id = a.employee_id
			left join tb_employees c on c.employee_id = a.approve_by
			where a.employee_id = '$uid'
			$sort LIMIT " . $limit . ",30";
        } else if ($type == 3)//editor
        {
            $perintah = "SELECT a.*,b.*,c.position as approval_position, a.status AS article_status  FROM tb_articles a
			left join tb_employees b on b.employee_id = a.employee_id
			left join tb_employees c on c.employee_id = a.approve_by
			where a.employee_id = '$uid'
			$sort LIMIT " . $limit . ",30";
        } else if ($type == 4)//redaktur
        {
            $perintah = "SELECT a.*,b.*,c.position as approval_position, a.status AS article_status  FROM tb_articles a
			left join tb_employees b on b.employee_id = a.employee_id
			left join tb_employees c on c.employee_id = a.approve_by
			where a.employee_id = '$uid'
			$sort LIMIT " . $limit . ",30";
        } else if ($type == 5)////chief-in editor
        {
            $perintah = "SELECT a.*,b.*,c.position as approval_position, a.status AS article_status FROM tb_articles a
			left join tb_employees b on b.employee_id = a.employee_id
			left join tb_employees c on c.employee_id = a.approve_by
			where a.employee_id = '$uid'
			$sort LIMIT " . $limit . ",30";
        } else if ($type == 6)//Creative Designer
        {
            $perintah = "SELECT a.*,b.*,c.position as approval_position, a.status AS article_status FROM tb_articles a
			left join tb_employees b on b.employee_id = a.employee_id
			left join tb_employees c on c.employee_id = a.approve_by
			$sort LIMIT " . $limit . ",30";
        }
        $hasil = $this->db->query($perintah);
        //pre($this->db->last_query());
        $hasil = $hasil->result_array();
        return $hasil;
    }

    public function listing_articles_approval($type, $sort, $limit)
    {
        $perintah = "";
        $uid = $this->session->userdata('uid');
        if ($type == 1) {
            $perintah = "SELECT a.*,b.*,c.position AS approval_position, a.status AS article_status FROM tb_articles a
			LEFT JOIN tb_employees b on b.employee_id = a.employee_id
			LEFT JOIN tb_employees c on c.employee_id = a.approve_by
			$sort LIMIT " . $limit . ",30";
        } else if ($type == 2)//reporter
        {
            $perintah = "SELECT a.*,b.*,c.position AS approval_position, a.status AS article_status FROM tb_articles a
			LEFT JOIN tb_employees b on b.employee_id = a.employee_id
			LEFT JOIN tb_employees c on c.employee_id = a.approve_by
			where a.employee_id = '$uid' and (a.status=0 or a.status=1 or a.status=6)
			$sort LIMIT " . $limit . ",30";
        } else if ($type == 3)//editor --1 --7
        {
            $perintah = "SELECT a.*,b.*,c.position AS approval_position, a.status AS article_status  FROM tb_articles a
			LEFT JOIN tb_employees b on b.employee_id = a.employee_id
			LEFT JOIN tb_employees c on c.employee_id = a.approve_by
			where (a.status!=0 and a.status!=3 and a.status!=4 and a.status!=5 and a.status!=6 and a.status!=8 and a.status!=9)
			or (a.status=1 or a.status=2 or a.status=7)
			or a.approve_editor = '$uid'
			$sort LIMIT " . $limit . ",30";
        } else if ($type == 4)//redaktur --2
        {
            $perintah = "SELECT a.*,b.*,c.position AS approval_position, a.status AS article_status  FROM tb_articles a
			LEFT JOIN tb_employees b on b.employee_id = a.employee_id
			LEFT JOIN tb_employees c on c.employee_id = a.approve_by
			where (a.status!=0 and a.status!=1 and (a.status=2 or a.status=3 or a.status=7 or a.status=8)
			and a.status!=4 and a.status!=5 and a.status!=6 and a.status!=9)
			or a.approve_editor = '$uid'
			$sort LIMIT " . $limit . ",30";
        } else if ($type == 5)////chief-in editor --3 --9
        {
            $perintah = "SELECT a.*,b.*,c.position AS approval_position, a.status AS article_status FROM tb_articles a
			LEFT JOIN tb_employees b on b.employee_id = a.employee_id
			LEFT JOIN tb_employees c on c.employee_id = a.approve_by
			where 
			(a.status!=0 and a.status!=1 and a.status!=2
			and a.status!=6 and a.status!=7)
			or (a.status=3 or a.status=4 or a.status=5 or a.status=8 or a.status=9)
			or a.approve_editor = '$uid'
			$sort LIMIT " . $limit . ",30";
        } else if ($type == 6)//Creative Designer
        {
            $perintah = "SELECT a.*,b.*,c.position AS approval_position, a.status AS article_status FROM tb_articles a
			LEFT JOIN tb_employees b on b.employee_id = a.employee_id
			LEFT JOIN tb_employees c on c.employee_id = a.approve_by
			where 
			(a.status!=0 and a.status!=1 and a.status!=2 and a.status!=3
			and a.status!=5 and a.status!=6 and a.status!=7)
			or (a.status=4 or a.status=8 or a.status=9)
			or a.approve_editor = '$uid' 
			$sort LIMIT " . $limit . ",30";
        }
        $hasil = $this->db->query($perintah);
        //pre($this->db->last_query());
        $hasil = $hasil->result_array();
        return $hasil;
    }

    public function get_employees()
    {
        $perintah = "SELECT * FROM tb_employees ORDER BY employee_id";
        $hasil = $this->db->query($perintah);
        $hasil = $hasil->result_array();
        return $hasil;
    }

    public function get_article_by_id($id)
    {
        $perintah = "SELECT * FROM tb_articles WHERE article_id='$id'";
        $hasil = $this->db->query($perintah);
        $hasil = $hasil->row();
        return $hasil;
    }

    public function get_photo_article($id)
    {
        $perintah = "SELECT a.*,b.* FROM tb_articles a
	                 LEFT JOIN tb_photo_articles b ON b.article_id = a.article_id
	                 WHERE a.article_id='$id'";
        $hasil = $this->db->query($perintah);
        $hasil = $hasil->result_array();
        return $hasil;
    }

    public function get_editor()
    {
        $perintah6 = "SELECT  * FROM tb_employees WHERE position='Editor' order BY name ASC ";
        $hasil6 = $this->db->query($perintah6);
        $hasil6 = $hasil6->result_array();

        $perintah7 = "SELECT  * FROM tb_employees WHERE position='Redaktur' order BY name ASC ";
        $hasil7 = $this->db->query($perintah7);
        $hasil7 = $hasil7->result_array();

        $perintah8 = "SELECT  * FROM tb_employees WHERE position='Editor-in Chief' order BY name ASC ";
        $hasil8 = $this->db->query($perintah8);
        $hasil8 = $hasil8->result_array();

        $perintah9 = "SELECT  * FROM tb_employees WHERE position='Creative Designer' order BY name ASC ";
        $hasil9 = $this->db->query($perintah9);
        $hasil9 = $hasil9->result_array();

        return array("hasil6" => $hasil6, "hasil7" => $hasil7, "hasil8" => $hasil8, "hasil9" => $hasil9);
    }

    public function delete($id)
    {
        $perintah = "DELETE FROM tb_articles WHERE article_id='$id'";
        $hasil = $this->db->query($perintah);
        return $hasil;
    }

    public function delete_photo($id)
    {
        $perintah = "DELETE FROM tb_photo_articles WHERE photo_article_id='$id'";
        $hasil = $this->db->query($perintah);
        return $hasil;
    }

    public function get_layout($article_id)
    {
        $perintah3 = "SELECT a.*,b.* FROM tb_articles a
                        LEFT JOIN tb_layout_articles b ON b.article_id  = a.article_id
                        WHERE a.article_id =$article_id ";
        $aksi3 = $this->db->query($perintah3);
        $aksi3 = $aksi3->result_array();

        /*$perintah4 = "SELECT a.*,c.* FROM tb_layout_articles_approve a
                        LEFT JOIN tb_layout_articles c ON c.layout_article_id = a.layout_article_id
                        WHERE a.layout_article_id=$layout_article_id";*/
        $perintah4 = "SELECT a.*,c.* FROM tb_layout_articles_approve a
                        LEFT JOIN tb_layout_articles c ON c.layout_article_id = a.layout_article_id
                        WHERE a.article_id =$article_id";
        $aksi4 = $this->db->query($perintah4);
        $aksi4 = $aksi4->result_array();

        /*$perintah = "SELECT c.fileout FROM tb_layout_articles_approve a
                        LEFT JOIN tb_layout_articles c ON c.layout_article_id = a.layout_article_id
                        WHERE a.layout_article_id=$layout_article_id and a.article_id =$article_id ";*/
        $perintah = "SELECT c.file FROM tb_layout_articles_approve a
                        LEFT JOIN tb_layout_articles c ON c.layout_article_id = a.layout_article_id
                        WHERE a.article_id =$article_id ";
        $hasil = $this->db->query($perintah);
        $hasil = $hasil->row();

        $perintah6 = "SELECT  * FROM tb_layout_articles WHERE article_id =$article_id  order BY layout_article_id ASC ";
        $hasil6 = $this->db->query($perintah6);
        $hasil6 = $hasil6->result_array();

        return array("aksi3" => $aksi3, "aksi4" => $aksi4, "hasil" => $hasil, "hasil6" => $hasil6);
    }

    public function search($article_date, $employee_id, $title)
    {
        $hasil = array();
        if ($article_date != "0-0-0" and $employee_id != "Select Author" and $title != "") {
            $perintah = "SELECT a.*,b.* FROM tb_articles a
                            LEFT JOIN tb_employees b ON b.employee_id = a.employee_id
                            WHERE a.article_date ='$article_date' AND a.employee_id ='$employee_id' AND a.title LIKE '%$title%'
                            ORDER BY a.article_date DESC ";
            $hasil = $this->db->query($perintah);
            $hasil = $hasil->result_array();
        } elseif ($article_date == "0-0-0" and $employee_id != "Select Author" and $title != "") {
            $perintah = "SELECT a.*,b.* FROM tb_articles a
                            LEFT JOIN tb_employees b ON b.employee_id = a.employee_id
                            WHERE a.employee_id ='$employee_id' AND a.title LIKE '%$title%'
                            ORDER BY a.article_date DESC ";
            $hasil = $this->db->query($perintah);
            $hasil = $hasil->result_array();
        } elseif ($article_date == "0-0-0" and $employee_id == "Select Author" AND $title != "") {
            $perintah = "SELECT a.*,b.* FROM tb_articles a
                            LEFT JOIN tb_employees b ON b.employee_id = a.employee_id
                            WHERE a.title LIKE '%$title%'
                            ORDER BY a.article_date DESC ";
            $hasil = $this->db->query($perintah);
            $hasil = $hasil->result_array();
        } elseif ($article_date == "0-0-0" and $employee_id == "Select Author" AND $title == "") {
            $perintah = "SELECT a.*,b.* FROM tb_articles a
                            LEFT JOIN tb_employees b ON b.employee_id = a.employee_id
                            ORDER BY a.article_date DESC ";
            $hasil = $this->db->query($perintah);
            $hasil = $hasil->result_array();
        } elseif ($article_date == "0-0-0" and $employee_id != "Select Author" AND $title == "") {
            $perintah = "SELECT a.*,b.* FROM tb_articles a
                            LEFT JOIN tb_employees b ON b.employee_id = a.employee_id
                            WHERE a.employee_id LIKE '%$employee_id%'
                            ORDER BY a.article_date DESC ";
            $hasil = $this->db->query($perintah);
            $hasil = $hasil->result_array();
        } elseif ($article_date == "0-0-0" and $employee_id == "Select Author" AND $title == "") {
            $perintah = "SELECT a.*,b.* FROM tb_articles a
                            LEFT JOIN tb_employees b ON b.employee_id = a.employee_id
                            ORDER BY a.article_date DESC ";
            $hasil = $this->db->query($perintah);
            $hasil = $hasil->result_array();
        }
        return $hasil;
    }

    public function save_article($article_date, $title, $content, $status, $approve_editor)
    {
        //cek agar data harus lengkap
        $uid = $this->session->userdata('uid');
        $perintah = "INSERT INTO tb_articles (
                                          employee_id
                                          , article_date
                                          , title
                                          , content
                                          , file
                                          , status
                                          , approve_editor
                                          , created_by
                                          , created_date
                                        )
		        VALUES
		                (
		                  '$uid'
		                  , '$article_date'
		                  , '$title'
		                  , '$content'
		                  , ''
		                  , $status
		                  , '$approve_editor'
		                  , '$uid'
		                  , now()
		                  )";

        $hasil = $this->db->query($perintah);
        //pre($this->db->last_query());
        $article_id = 0;
        if ($hasil) {
            $perintah = "SELECT MAX(article_id) AS article_id FROM tb_articles";
            $hasil = $this->db->query($perintah);
            $hasil = (array)$hasil->row();
            $article_id = $hasil['article_id'];
        }
        return $article_id;
    }

    public function get_position_editor()
    {
        $perintah = "SELECT * FROM tb_employees WHERE position='Editor' ORDER BY name ASC";
        $hasil = $this->db->query($perintah);
        $hasil = $hasil->result_array();
        return $hasil;
    }

    public function update_article($id, $article_date, $title, $content, $approve_editor)
    {
        $perintah = "UPDATE tb_articles SET article_date='$article_date',title='$title',content='$content', approve_editor='$approve_editor',updated_date=now() WHERE article_id=$id";
        $hasil = $this->db->query($perintah);
        return $hasil;
    }

    public function delete_article_detail($id)
    {
        $perintah = "SELECT a.*,b.name FROM tb_articles a
                        LEFT JOIN tb_employees b ON b.employee_id = a.approve_editor
                        WHERE article_id=$id";
        $hasil = $this->db->query($perintah);
        $hasil = $hasil->row();
        return $hasil;
    }

    public function delete_photo_detail($id)
    {
        $perintah = "SELECT * FROM tb_photo_articles WHERE photo_article_id=$id";
        $hasil = $this->db->query($perintah);
        $hasil = $hasil->row();
        return $hasil;
    }

    public function get_decline($article_id)
    {
        $perintah = "SELECT a.*,b.* FROM tb_articles a
                        LEFT JOIN tb_employees b ON b.employee_id = a.employee_id
                        WHERE article_id= '$article_id'";
        $hasil = $this->db->query($perintah);
        $hasil = $hasil->result_array();
        $position = 0;
        foreach ($hasil AS $row) {
            $position = $row['position'];
        }
        return $position;
    }

    public function update_status_article($article_id, $status, $note)
    {
        $perintah = "UPDATE tb_articles SET status='$status',note='$note',updated_date=now() WHERE article_id=$article_id";
        $hasil = $this->db->query($perintah);
        return $hasil;
    }

    public function save_layout_article_approve($article_id, $layout_article_id)
    {
        $uid = $this->session->userdata('uid');
        $perintah = "INSERT INTO tb_layout_articles_approve (article_id, layout_article_id, created_by, created_date)
		VALUES ('$article_id','$layout_article_id','$uid', now())";
        $hasil = $this->db->query($perintah);
        return $hasil;
    }

    public function save_layout_article($article_id, $file)
    {
        $uid = $this->session->userdata('uid');
        $perintah = "INSERT INTO tb_layout_articles (article_id, file, created_by, created_date)
		VALUES ('$article_id','$file','$uid', now())";
        $hasil = $this->db->query($perintah);
        return $hasil;
    }

    public function save_photo_article($article_id, $file)
    {
        $uid = $this->session->userdata('uid');
        $perintah = "INSERT INTO tb_photo_articles (article_id, file, created_by, created_date)
		VALUES ('$article_id','$file','$uid', now())";
        $hasil = $this->db->query($perintah);
        return $hasil;
    }

    public function get_email_template($type_id)
    {
        $perintah = "SELECT * FROM tb_email_templates where type_id='$type_id'";
        $hasil = $this->db->query($perintah);
        $hasil = $hasil->result_array();
        return $hasil[0];
    }

    public function update_status_article_email($article_id, $status, $approve_by = 0)
    {
        $perintah = "UPDATE tb_articles SET status='$status',approve_by=$approve_by,updated_date=now() WHERE article_id=$article_id";
        $hasil = $this->db->query($perintah);
        return $hasil;
    }

    public function total_layouts_by_id($article_id)
    {
        $a = "SELECT COUNT(layout_article_id) AS total FROM tb_layout_articles WHERE article_id='$article_id'";
        $b = $this->db->query($a);
        $b = $b->result_array();
        $c = $b[0]["total"];
        return $c;
    }

}
