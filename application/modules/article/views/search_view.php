<?php echo js('select.js'); ?>
<div id="content">
    <div class="content-detail">
        <p align="right">
            <strong>Login User : </strong>
            <?php echo $this->session->userdata('nama'); ?><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                Position: </strong><?php echo $this->session->userdata('posisi'); ?>
        </p>
        <form action="<?php echo site_url('article/search_process') ?>" method=post>
            <fieldset>
                <legend>Search Article</legend>
                <b>Date : &nbsp; &nbsp;</b>
                <?php
                echo "<SELECT NAME= tgl>";
                echo "<OPTION VALUE=0 SELECTED >Date";
                $tgl = '';
                $bln = '';
                $thn = '';
                //Bentuk pilihan tanggal 0 sampai dengan 31
                for ($i = 1; $i < 32; $i++) {
                    if ($tgl == $i)
                        $sel = "SELECTED";
                    else
                        $sel = "";
                    print("<OPTION VALUE=\"$i\" $sel>$i");
                }
                echo "</OPTION></SELECT>";
                echo "<SELECT NAME=bln>";
                echo "<OPTION VALUE=0 SELECTED >Month";
                for ($i = 1; $i < 13; $i++) {
                    $sel[$i] = "";

                    if ($i == $bln)
                        $sel[$i] = "SELECTED";
                }
                print("<OPTION VALUE=\"1\"  $sel[1]>Januari  ");
                print("<OPTION VALUE=\"2\"  $sel[2]>Februari ");
                print("<OPTION VALUE=\"3\"  $sel[3]>Maret    ");
                print("<OPTION VALUE=\"4\"  $sel[4]>April    ");
                print("<OPTION VALUE=\"5\"  $sel[5]>Mei      ");
                print("<OPTION VALUE=\"6\"  $sel[6]>Juni     ");
                print("<OPTION VALUE=\"7\"  $sel[7]>Juli     ");
                print("<OPTION VALUE=\"8\"  $sel[8]>Agustus  ");
                print("<OPTION VALUE=\"9\"  $sel[9]>September");
                print("<OPTION VALUE=\"10\" $sel[10]>Oktober ");
                print("<OPTION VALUE=\"11\" $sel[11]>November");
                print("<OPTION VALUE=\"12\" $sel[12]>Desember");
                echo "</OPTION></SELECT>";
                echo "<SELECT NAME=thn>";
                echo "<OPTION VALUE=0 SELECTED >Year";
                // Bentuk pilihan tahun dari 1900
                // sampai sekarang
                $sekarang = (integer)date("Y");
                for ($i = 1980; $i <= $sekarang; $i++) {
                    if ($i == $thn)
                        $sel = "SELECTED";
                    else
                        $sel = "";
                    print("<OPTION VALUE=\"$i\" $sel>$i");
                }
                echo "</OPTION></SELECT>"; ?>
                <b>&nbsp; &nbsp;&nbsp; &nbsp;Author :&nbsp; &nbsp;</b>
                <select id="user_id" name="user_id">
                    <option selected="Select Author">Select Author</option>
                    <?php
                    foreach ($data_employees as $row5) {
                        echo "<optiON value='$row5[employee_id]'>$row5[name]</optiON>";
                    }
                    ?>
                </select>
                <b>&nbsp; &nbsp;&nbsp; &nbsp;Enter The Word/Article Title :&nbsp; &nbsp;</b>
                <input type=text name=judul_artikel size=30>&nbsp;&nbsp;&nbsp;&nbsp;
                <input type=submit value=Search>
                <input type="button" value="Back" onClick="window.location = '<?php echo site_url('article/approval') ?>'"/>
            </fieldset>
        </form>
    </div>
</div>