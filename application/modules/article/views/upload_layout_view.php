<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<?php echo js('script.js'); ?>
<?php echo js('article_upload_layout.js'); ?>
<div id="content">
    <div class="content-detail">
        <p align="right">
            <strong>Login User : </strong>
            <?php echo $this->session->userdata('nama'); ?><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                Position: </strong><?php echo $this->session->userdata('posisi'); ?>
        </p>
        <? if ($this->session->userdata('type') != 5) { ?>
            <form id="form1" name="form1" enctype="multipart/form-data" method="post" action="<?php echo site_url('article/upload_layout_process') ?>">
                <input type="hidden" name="id" value="<?php echo $detail["article_id"]; ?>">
                <fieldset>
                    <legend>Layout Upload</legend>
                    <br>
                    <input name="layout" type="file" id="layout"/>
                    <br>
                    <br>
                    <input type=submit value=Upload>
                </fieldset>
            </form>
        <? } ?>
        <br>
        <table class="TFtable">
            <tr>
                <td><b>No</b></td>
                <td><b>Layout</b></td>
                <td colspan="2" align="center"><b>Action</b></td>
            </tr>
            <?
            $url_detail = site_url('layout/process');
            $url_delete = site_url('article/delete_layout');
            $no = 1;
            echo "<font color='blue'>APPROVE LAYOUT : " . $detail['file'] . '</font>';
            foreach ($aksi3 as $data3):
            ?>
            <tr>
                <td>
                    <?php echo $no; ?>
                </td>
                <? echo "<td>$data3[file]</td>"; ?>
                <? echo "<td><center><a href='$url_detail?id=$data3[layout_article_id]'>VIEW</a></td>"; ?>
                <? echo "<td align='center'><a href=\"$url_delete?id=$data3[layout_article_id]\">Delete</a></td></tr>" ?>
                <?php
                $no++;
                endforeach;
                ?>
        </table>
    </div>
    <br>
    <? if ($this->session->userdata('type') != 5) { ?>
        <form method="link" action="<?php echo site_url('article/approval') ?>"><input type="submit" value="Back"/></form>
    <? } ?>
    <br>
    <br>
    <? if ($this->session->userdata('type') == 5 or $this->session->userdata('type') == 1) { ?>
        <form name="form3" method="post" action="<?php echo site_url('article/select_layout_process') ?>" onsubmit="return validate()">
            <input type="hidden" name="id" value="<?php echo $this->input->get('id') ?>">
            <fieldset>
                <legend>Select Layout</legend>
                <br>
                <select id="layout_article_id" name="layout_article_id">
                    <option selected="Select Layout">Select Layout</option>
                    <?php
                    foreach ($hasil6 as $row6) {
                        echo "<option value=" . $row6['layout_article_id'] . ">" . $row6['file'] . "</option>";
                        echo "</option>";
                    }
                    ?>
                </select>
                <input type=submit value=Select>
            </fieldset>
        </form>
        <br>
        <form method="link" action="<?php echo site_url('article/approval') ?>"><input type="submit" value="Back"/></form>
    <? } ?>
</div>