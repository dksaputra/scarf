<?php echo js('ckeditor/ckeditor.js'); ?>
<?php echo js('ckeditor/style.js'); ?>
<?php echo js('script.js'); ?>
<?php echo js('jquery-1.2.3.min.js'); ?>
<?php echo js('article_create.js'); ?>
<?php echo js('select.js'); ?>
<style>
    #previewimg1 {
        max-width: 250px;
        max-height: 200px;
    }

    #previewimg2 {
        max-width: 250px;
        max-height: 200px;
    }

    #previewimg3 {
        max-width: 250px;
        max-height: 200px;
    }

    #previewimg4 {
        max-width: 250px;
        max-height: 200px;
    }

    #previewimg5 {
        max-width: 250px;
        max-height: 200px;
    }

    #previewimg6 {
        max-width: 250px;
        max-height: 200px;
    }

    #previewimg7 {
        max-width: 250px;
        max-height: 200px;
    }

    #previewimg6 {
        max-width: 250px;
        max-height: 200px;
    }

    #previewimg8 {
        max-width: 250px;
        max-height: 200px;
    }

    #previewimg6 {
        max-width: 250px;
        max-height: 200px;
    }

    #previewimg9 {
        max-width: 250px;
        max-height: 200px;
    }

    #previewimg10 {
        max-width: 250px;
        max-height: 200px;
    }
</style>
<div id="content">
    <div class="content-detail">
        <p align="right">
            <strong>Login User : </strong>
            <?php echo $this->session->userdata('nama'); ?><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                Position: </strong><?php echo $this->session->userdata('posisi'); ?>
        </p>
        <form name="form1" method="post" action="<?php echo site_url('article/save') ?>" enctype="multipart/form-data"
              onsubmit="return validate()">
            <fieldset>
                <legend>Input Article</legend>
                Date
                <br>
                <?php
                $date = date('d');
                echo "<SELECT NAME= tgl>";
                echo "<OPTION VALUE=0 SELECTED >Date";
                $tgl = '';
                $bln = '';
                $thn = '';
                //Bentuk pilihan tanggal 0 sampai dengan 31
                for ($i = 1; $i < 32; $i++) {
                    if ($date == $i)
                        $sel = "SELECTED";
                    else
                        $sel = "";

                    print("<OPTION VALUE=\"$i\" $sel>$i");
                }
                echo "</OPTION></SELECT>";

                $month = date('m');
                echo "<SELECT NAME=bln>";
                echo "<OPTION VALUE=0 SELECTED >Month";
                for ($i = 1; $i < 13; $i++) {
                    $sel[$i] = "";

                    if ($i == $month)
                        $sel[$i] = "SELECTED";
                }
                print("<OPTION VALUE=\"1\"  $sel[1]>Januari  ");
                print("<OPTION VALUE=\"2\"  $sel[2]>Februari ");
                print("<OPTION VALUE=\"3\"  $sel[3]>Maret    ");
                print("<OPTION VALUE=\"4\"  $sel[4]>April    ");
                print("<OPTION VALUE=\"5\"  $sel[5]>Mei      ");
                print("<OPTION VALUE=\"6\"  $sel[6]>Juni     ");
                print("<OPTION VALUE=\"7\"  $sel[7]>Juli     ");
                print("<OPTION VALUE=\"8\"  $sel[8]>Agustus  ");
                print("<OPTION VALUE=\"9\"  $sel[9]>September");
                print("<OPTION VALUE=\"10\" $sel[10]>Oktober ");
                print("<OPTION VALUE=\"11\" $sel[11]>November");
                print("<OPTION VALUE=\"12\" $sel[12]>Desember");
                echo "</OPTION></SELECT>";
                $year = date('Y');
                echo "<SELECT NAME=thn>";
                echo "<OPTION VALUE=0 SELECTED >Year";
                // Bentuk pilihan tahun dari 1900
                // sampai sekarang
                $sekarang = (integer)date("Y");
                for ($i = 1980; $i <= $sekarang; $i++) {
                    if ($i == $year)
                        $sel = "SELECTED";
                    else
                        $sel = "";

                    print("<OPTION VALUE=\"$i\" $sel>$i");
                }
                echo "</OPTION></SELECT>";
                ?>
                <br>
                <br>
                Tittle
                <br>
                <input type="text" name="title" size=70 maxlength=70>
                <br>
                <br>
                Content
                <br>
                <textarea name="content" class="ckeditor"></textarea>
                <br>
                <? if ($this->session->userdata('type') == 2 or $this->session->userdata('type') == 1) { ?>
                    Approval Editor
                    <br>
                    <select id="approve_editor" name="approve_editor">
                        <option selected="Select Redaktur">Select Editor</option>
                        <?php
                        foreach ($hasil6 as $row6) {
                            echo "<option value=" . $row6['employee_id'] . ">" . $row6['name'] . "</option>";
                            echo "</option>";
                        }
                        ?>
                    </select>
                <? } else if ($this->session->userdata('type') == 3) { ?>
                    Approval Redaktur
                    <br>
                    <select id="approve_editor" name="approve_editor">
                        <option selected="Select Redaktur">Select Redaktur</option>
                        <?php
                        foreach ($hasil7 as $row7) {
                            echo "<option value=" . $row7['employee_id'] . ">" . $row7['name'] . "</option>";
                            echo "</option>";
                        }
                        ?>
                    </select>
                <? } else if ($this->session->userdata('type') == 4) { ?>
                    Approval Editor-in Chief
                    <br>
                    <select id="approve_editor" name="approve_editor">
                        <option selected="Select Editor-in Chief">Select Editor-in Chief</option>
                        <?php
                        foreach ($hasil8 as $row8) {
                            echo "<option value=" . $row8['employee_id'] . ">" . $row8['name'] . "</option>";
                            echo "</option>";
                        }
                        ?>
                    </select>
                <? } else if ($this->session->userdata('type') == 5) { ?>
                    Approval Creative Designer
                    <br>
                    <select id="approve_editor" name="approve_editor">
                        <option selected="Select Creative Designer">Select Creative Designer</option>
                        <?php
                        foreach ($hasil9 as $row9) {
                            echo "<option value=" . $row9['employee_id'] . ">" . $row9['name'] . "</option>";
                            echo "</option>";
                        }
                        ?>
                    </select>
                <? } ?>
            </fieldset>
            <br>
            <fieldset>
                <legend>Upload Photo</legend>
                <div id="maindiv">
                    <div id="formdiv">
                        <br>
                        <div id="filediv"><input name="foto" type="file" id="file"></div>
                        <br/>
                        <!--<input type="button" id="add_more" class="upload" value="Add More Files"/>-->
                        Note : <font color="blue">First Field is Compulsory. Only JPEG,PNG,JPG Type Image
                            Uploaded.</font>
                        <br/>
                        <br/>
                    </div>
                </div>
            </fieldset>
            <br>
            <input type=submit name="submit" value=Submit>
            <input type=reset value=Clear>
            <input type="button" value="Back" onClick="window.location = '<?php echo site_url('article/approval') ?>'"/>
            <br><br>
        </form>
    </div>
</div>