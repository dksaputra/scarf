<?php echo js('ckeditor/ckeditor.js'); ?>
<?php echo js('ckeditor/style.js'); ?>
<?php echo js('script.js'); ?>
<?php echo js('jquery-1.2.3.min.js'); ?>
<?php echo js('article_create.js'); ?>
<?php echo js('select.js'); ?>
<div id="content">
    <div class="content-detail">
        <p align="right">
            <strong>Login User : </strong>
            <?php echo $this->session->userdata('nama'); ?><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                Position: </strong><?php echo $this->session->userdata('posisi'); ?>
        </p>
        <form name=form1 method=post action="<?php echo site_url('article/update') ?>" enctype="multipart/form-data">
            <fieldset>
                <legend>Article Edit</legend>
                <input type="hidden" name="id" value="<?php echo "$detail[article_id]" ?>">
                Date
                <br>
                <input type="text" name="article_date" value="<?php echo "$detail[article_date]" ?>" size=70
                       maxlength=70>
                <br>
                <br>
                Title
                <br>
                <input type="text" name="title" value="<?php echo "$detail[title]" ?>" size=70
                       maxlength=70>
                <br>
                <br>
                Content
                <br>
                <textarea name="content" class="ckeditor"><?php echo "$detail[content]" ?></textarea>
                <br>
                Approval Editor
                <br>
                <select name="approve_editor">
                    <?php
                    foreach ($editor as $data2) {
                        if ($detail['approve_editor'] == $data2['employee_id']) {
                            $terpilih = "selected";
                        } else {
                            $terpilih = "";
                        }
                        echo "<option value=" . $data2['employee_id'] . " $terpilih>" . $data2['name'] . "</option>";
                    }
                    ?>
                </select>
            </fieldset>
            <br><br>
            <input type=submit value=Save>
            <input type="button" value="Cancel" onClick="window.location = '<?php echo site_url('article/approval') ?>'"/>
            <br><br>
        </form>
    </div>
</div>