<div id="content">
    <p align="right">
        <strong>Login User : </strong>
        <?php echo $this->session->userdata('nama'); ?><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            Position: </strong><?php echo $this->session->userdata('posisi'); ?>
    </p>
    <form action="<?php echo site_url('article/search_process'); ?>" method=post>
        <fieldset>
            <legend>Search Article</legend>
            <b>Date : &nbsp; &nbsp;</b>
            <?php
            echo "<SELECT NAME= tgl>";
            echo "<OPTION VALUE=0 SELECTED >Date";
            $tgl = '';
            $bln = '';
            $thn = '';
            //Bentuk pilihan tanggal 0 sampai dengan 31
            for ($i = 1; $i < 32; $i++) {
                if ($tgl == $i)
                    $sel = "SELECTED";
                else
                    $sel = "";
                print("<OPTION VALUE=\"$i\" $sel>$i");
            }
            echo "</OPTION></SELECT>";
            echo "<SELECT NAME=bln>";
            echo "<OPTION VALUE=0 SELECTED >Month";
            for ($i = 1; $i < 13; $i++) {
                $sel[$i] = "";
                if ($i == $bln)
                    $sel[$i] = "SELECTED";
            }
            print("<OPTION VALUE=\"1\"  $sel[1]>Januari  ");
            print("<OPTION VALUE=\"2\"  $sel[2]>Februari ");
            print("<OPTION VALUE=\"3\"  $sel[3]>Maret    ");
            print("<OPTION VALUE=\"4\"  $sel[4]>April    ");
            print("<OPTION VALUE=\"5\"  $sel[5]>Mei      ");
            print("<OPTION VALUE=\"6\"  $sel[6]>Juni     ");
            print("<OPTION VALUE=\"7\"  $sel[7]>Juli     ");
            print("<OPTION VALUE=\"8\"  $sel[8]>Agustus  ");
            print("<OPTION VALUE=\"9\"  $sel[9]>September");
            print("<OPTION VALUE=\"10\" $sel[10]>Oktober ");
            print("<OPTION VALUE=\"11\" $sel[11]>November");
            print("<OPTION VALUE=\"12\" $sel[12]>Desember");
            echo "</OPTION></SELECT>";
            echo "<SELECT NAME=thn>";
            echo "<OPTION VALUE=0 SELECTED >Year";
            // Bentuk pilihan tahun dari 1900
            // sampai sekarang
            $sekarang = (integer)date("Y");
            for ($i = 1980; $i <= $sekarang; $i++) {
                if ($i == $thn)
                    $sel = "SELECTED";
                else
                    $sel = "";

                print("<OPTION VALUE=\"$i\" $sel>$i");
            }
            echo "</OPTION></SELECT>"; ?>
            <b>&nbsp; &nbsp;&nbsp; &nbsp;Author :&nbsp; &nbsp;</b>
            <select id="employee_id" name="employee_id">
                <option selected="Select Author">Select Author</option>
                <?php
                foreach ($data_employees as $row5) {
                    echo "<option value='$row5[article_id]'>$row5[name]";
                    echo "</option>";
                }
                ?>
            </select>
            <b>&nbsp; &nbsp;&nbsp; &nbsp;Enter The Word/Article Title :&nbsp; &nbsp;</b>
            <input type=text name=judul_artikel size=30>&nbsp;&nbsp;&nbsp;&nbsp;
            <input type=submit value=Search>
        </fieldset>
    </form>
    <br><br>
    <strong>My Article List :</strong>
    <br><br>
    <table class="TFtable">
        <tr>
            <th align="center">Date<br><a href="index.php?sort=asc"><b><-ASC-></b></a><a href="index.php?sort=desc"><b><-DESC-></b></a>
            </th>
            <td><b>Author</b></td>
            <td><b>Title</b></td>
            <td><b>Status</b></td>
            <td><b>Last Approval/Decline</b></td>
            <td><b>Note</b></td>
            <? $x = 0 ?>
            <? if ($this->session->userdata('type') == 1) {
                $x = 6;
            } ?>
            <? if ($this->session->userdata('type') == 2) {
                $x = 5;
            } ?>
            <? if ($this->session->userdata('type') == 3) {
                $x = 3;
            } ?>
            <? if ($this->session->userdata('type') == 4) {
                $x = 3;
            } ?>
            <? if ($this->session->userdata('type') == 5) {
                $x = 5;
            } ?>
            <? if ($this->session->userdata('type') == 6) {
                $x = 4;
            } ?>
            <td colspan="<?= $x; ?>" align="center"><b>Action</b></td>
        </tr>
        <?php
        foreach ($data_articles as $row) {
            //pre($row);
            /*$link = "";
            if ($this->session->userdata('type') == 1 and $row['article_status'] == 0) {
                $link = "email_editor.php";
            }//reporter-redaktur
            else if ($this->session->userdata('type') == 1 and $row['article_status'] == 1) {
                $link = "email_redaktur.php";
            }//reporter-redaktur
            else if ($this->session->userdata('type') == 1 and $row['article_status'] == 2) {
                $link = "email_artistik.php";
            }//reporter-redaktur
            else if ($this->session->userdata('type') == 1 and $row['article_status'] == 3) {
                $link = "email_editorinchief.php";
            }//reporter-redaktur
            else if ($this->session->userdata('type') == 3) {
                $link = "email_redaktur.php";
            }//editor-redaktur
            else if ($this->session->userdata('type') == 4) {
                $link = "email_editorinchief.php";
            }//redaktur-chief
            else if ($this->session->userdata('type') == 5 and $row['article_status'] == 9) {
                $link = "email_editorinchief-all.php";
            }//chief to all
            else if ($this->session->userdata('type') == 5) {
                $link = "email_artistik.php";
            }//chief-Creative Designer
            else if ($this->session->userdata('type') == 6) {
                $link = "email_editorinchief_layout.php";
            }//aristik-chief*/


            $link = site_url('article/send_email_process') . '?id=' . $row['article_id'] . '&type=' . $this->session->userdata('type') . '&status=' . $row['article_status'];
            $count = 0;
            $index = ($count + 1);
            if ($count % 2 == 1) {
                $style = "row1";
            } else {
                $style = "row2";
            }
            echo "<tr class='" . $style . "'>";
            $pecah_tgl_artikel = "";
            $pecah_tgl_artikel = explode("-", $row["article_date"]);
            $tgl_artikel = $pecah_tgl_artikel[2] . "-" . $pecah_tgl_artikel[1] . "-" . $pecah_tgl_artikel[0];
            echo "<td>$tgl_artikel</td>";
            echo "<td>$row[name]</td>";
            echo "<td>$row[title]</td>";
            if ($row['article_status'] == 0) {
                $s = "Send Email to Editor First";
            }
            if ($row['article_status'] == 1) {
                $s = "Waiting for Editor approval";
            }
            if ($row['article_status'] == 2) {
                $s = "Waiting for Redaktur approval";
            }
            if ($row['article_status'] == 3) {
                $s = "Waiting for Editor-in Chief approval";
            }
            if ($row['article_status'] == 4) {
                $s = "Waiting for Creative Designer approval";
            }
            if ($row['article_status'] == 5) {
                $s = "Approved by Editor-in Chief";
            }
            if ($row['article_status'] == 6) {
                $s = "<font color='red'>Decline Reporter</font>";
            }
            if ($row['article_status'] == 7) {
                $s = "<font color='red'>Decline Editor</font>";
            }
            if ($row['article_status'] == 8) {
                $s = "<font color='red'>Decline Redaktur</font>";
            }
            if ($row['article_status'] == 9) {
                $s = "Waiting for Editor-in Chief approval for Layout";
            }
            echo "<td>$s</td>";
            echo "<td>$row[note]</td>";
            $url_detail = site_url('article/detail');
            $url_upload = site_url('article/upload_layout');
            $url_edit = site_url('article/edit');
            $url_delete = site_url('article/delete');
            echo "<td align='center'><a href=\"$url_detail?id=$row[article_id]\">VIEW AND SEND</a></td>";
            /*
               Status Artikel :
               0 Send Email to Editor First
               1 Waiting for Editor approval
               2 Waiting for Redaktur approval
               3 Waiting for Editor-in Chief  approval
               4 Waiting for Artistik approval
               5 Approved by Editor-in Chief
               6 Decline Reporter
               7 Decline Editor
               8 Decline/Approve Artistik
               9 Waiting for Editor-in Chief approval for Layout
            */
            if ($this->session->userdata('type') == 1) {
                echo "<td align='center'><a href=\"$url_upload?id=$row[article_id]\">LAYOUT</a></td>";
                echo "<td align='center'><a href=\"$url_edit?id=$row[article_id]\">EDIT</a></td>";
                //echo "<td align='center'><a href=\"$link\">EMAIL</a></td>";
                echo "<td align='center'><a href=\"$url_delete?id=$row[article_id]\">DELETE</a></td>";
            } else if ($this->session->userdata('type') == 2 and $row['employee_id'] == $this->session->userdata('uid') and ($row['article_status'] == 0 or $row['article_status'] == 1
                or $row['article_status'] == 6) and $row['article_status'] != 2 and $row['article_status'] != 3 and $row['article_status'] != 4
                and $row['article_status'] != 5 and $row['article_status'] != 7 and $row['article_status'] != 8
            ) {//REPORTER
                echo "<td align='center'><a href=\"$url_edit?id=$row[article_id]\">EDIT</a></td>";
                echo "<td align='center'><a href=\"$url_delete?id=$row[article_id]\">DELETE</a></td>";
            } else if ($this->session->userdata('type') == 3 and $row['employee_id'] == $this->session->userdata('uid') and $row['article_status'] != 0
                and ($row['article_status'] == 1 or $row['article_status'] == 2 or $row['article_status'] == 7) and $row['article_status'] != 3 and $row['article_status'] != 4
                and $row['article_status'] != 5 and $row['article_status'] != 6 and $row['article_status'] != 8
            ) {//EDITOR
                echo "<td align='center'><a href=\"$url_edit?id=$row[article_id]\">EDIT</a></td>";
            } else if ($this->session->userdata('type') == 4 and $row['employee_id'] == $this->session->userdata('uid') and $row['article_status'] != 0
                and $row['article_status'] != 1 and ($row['article_status'] == 2 or $row['article_status'] == 3)
                and $row['article_status'] != 4 and $row['article_status'] != 5 and $row['article_status'] != 6 and $row['article_status'] != 7
                and $row['article_status'] != 8
            ) {//REDAKTUR
                echo "<td align='center'><a href=\"$url_edit?id=$row[article_id]\">EDIT</a></td>";
            } else if ($this->session->userdata('type') == 5 and $row['employee_id'] == $this->session->userdata('uid') and $row['article_status'] != 0 and $row['article_status'] != 1
                and $row['article_status'] != 2 and ($row['article_status'] == 3 or $row['article_status'] == 5)
                and $row['article_status'] != 6 and $row['article_status'] != 7 and $row['article_status'] != 8
            ) {//CHIEF
                echo "<td align='center'><a href=\"$url_edit?id=$row[article_id]\">EDIT</a></td>";
            } else if ($this->session->userdata('type') == 5 and $row['employee_id'] == $this->session->userdata('uid') and $row['article_status'] != 0 and $row['article_status'] != 1
                and $row['article_status'] != 2 and ($row['article_status'] == 5 or $row['article_status'] == 9 or $row['article_status'] == 4 or $row['article_status'] == 8)
                and $row['article_status'] != 6 and $row['article_status'] != 7 and $row['article_status'] != 8
            ) {//CHIEF
                echo "<td align='center'><a href=\"$url_upload?id=$row[article_id]\">LAYOUT</a></td>";
                echo "<td align='center'><a href=\"$url_edit?id=$row[article_id]\">EDIT</a></td>";
            } else if ($this->session->userdata('type') == 6 and $row['article_status'] != 0 and $row['article_status'] != 1
                and $row['article_status'] != 2 and ($row['article_status'] == 4 or $row['article_status'] == 8)
                and $row['article_status'] != 5 and $row['article_status'] != 6 and $row['article_status'] != 7
            ) {//Creative Designer
                echo "<td align='center'><a href=\"$url_upload?id=$row[article_id]\">LAYOUT</a></td>";
            }
            echo "</tr>";
            $count++;
        }
        ?>
    </table>
</div>
<br>
<tr class="thstyle">
    <td class="thbottom" colspan="8">
        <?php
        if ($hal > 1) {
            echo "<a href='?page=" . ($hal - 1) . "&count=" . (($hal * 5) - 30) . "'><b>&lt;&lt;Previous</b></a>";
        } else {
            echo "&lt;&lt;Previous";
        }

        if ($hal < $d) {
            echo "&nbsp;&nbsp;&nbsp;<a href='?page=" . ($hal + 1) . "&count=" . ($hal * 30) . "'><b>Next&gt;&gt;</b></a>";
        } else {
            echo "&nbsp;&nbsp;&nbsp;Next&gt;&gt;";
        }
        ?></td>
</tr>
<tr>
    <td colspan="8" class="thbtm_hasil">Total : <b><?php echo $c; ?></b></td>
</tr>
</table>
<span style="float:left;padding-left:0px;"><b><?php echo "Page :&nbsp;" . $hal; ?></b></span><br><br>