<link rel="stylesheet" type="text/css" href="css/style.css"/>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.lightbox-0.5.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/jquery.lightbox-0.5.css" media="screen"/>
<script type="text/javascript">
    $(function () {
        $('#gallery a').lightBox();
    });
</script>
<style>
    #lightbox-image {
        max-width: 750px;
        max-height: 600px;
    }

    #lightbox-container-image-box {
        max-width: 800px;
        max-height: 600px;
    }

    #lightbox-container-image-data-box {
        max-width: 780px;
    }

    #jquery-overlay {
        width: 1349px;
        height: 2605px;
    "
    }

    #lightbox-nav-btnNext {
        max-width: 1780px;
        max-width: 780px;
    }
</style>
<div id="content">
    <div class="content-detail">
        <p align="right">
            <strong>Login User : </strong>
            <?php echo $this->session->userdata('nama'); ?><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                Position: </strong><?php echo $this->session->userdata('posisi'); ?>
        </p>
        <div class="row-fluid">
            <fieldset>
                <legend>Article Content</legend>
                <h2><?= $detail['title']; ?></h2>
                <p style="text-align:justify;"><?= substr($detail['content'], 0); ?></p>
            </fieldset>
        </div>
    </div>
    <?
    $link = "";
    if ($this->session->userdata('type') == 2) {
        $type_id = 1;
        $status = 1;
    }//reporter-editor
    else if ($this->session->userdata('type') == 3) {
        $type_id = 2;
        $status = 2;
    }//editor-redaktur
    else if ($this->session->userdata('type') == 4) {
        $type_id = 4;
        $status = 3;
    }//redaktur-chief
    else if ($this->session->userdata('type') == 5 and $detail['status'] == 9) {
        $type_id = 5;
        $status = 5;
    }//chief to all
    else if ($this->session->userdata('type') == 5) {
        $type_id = 3;
        $status = 4;
    }//chief-artistik
    else if ($this->session->userdata('type') == 6) {
        $type_id = 4;
        $status = 9;
    }//aristik-chief

    $this->load->view('_inc_photo');

    $link = site_url('article/send_email_process');
    if ($this->session->userdata('type') == 2 and ($detail['status'] == 0 or $detail['status'] == 6)){
    ?><!--REPORTER-->
    <!--
       Status Artikel :
       0 Send Email to Editor First
       1 Waiting for Editor approval
       2 Waiting for Redaktur approval
       3 Waiting for Editor-in Chief  approval
       4 Waiting for Creative Designer approval
       5 Approved by Editor-in Chief
       6 Decline Reporter
       7 Decline Editor
       8 Decline/Approve Creative Designer
    -->
    <form name="form1" method="post" action=<?= $link; ?> enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?= $detail['article_id']; ?>">
        <input type="hidden" name="type" value="<?= $type_id; ?>">
        <input type="hidden" name="status" value="<?= $status; ?>">
        <input type=submit value="Send Email to Editor">
        <br><br>
    </form>
    <br><br>
    <? } ?>
    <? if ($this->session->userdata('type') == 3 and ($detail['status'] == 1 or $detail['status'] == 7)){ ?><!--EDITOR-->
    <form name="form1" method="post" action=<?= $link; ?> enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?= $detail['article_id']; ?>">
        <input type="hidden" name="type" value="<?= $type_id; ?>">
        <input type="hidden" name="status" value="<?= $status; ?>">
        <input type=submit value="Approve and Send Email to Redaktur">
        <br><br>
    </form>
<br><br>
<? if ($detail['status'] != 7 and $detail['employee_id'] != $this->session->userdata('uid')) { ?>
    <form name="form2" method="post" action="<?php echo site_url('article/decline_process'); ?>" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?= $detail['article_id']; ?>">
        <input type="hidden" name="type" value="<?= $type_id; ?>">
        <input type="hidden" name="status" value="6">
        Decline Reason :<br><textarea name="note" rows="4" cols="50"></textarea>
        <input type=submit value="Decline Article">
        <br><br>
    </form>
<? } ?>
<? } ?>
    <? if ($this->session->userdata('type') == 4 and ($detail['status'] == 2 or $detail['status'] == 8)){ ?><!--REDAKTUR-->
    <form name="form1" method="post" action=<?= $link; ?> enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?= $detail['article_id']; ?>">
        <input type="hidden" name="type" value="<?= $type_id; ?>">
        <input type="hidden" name="status" value="<?= $status; ?>">
        <input type=submit value="Approve and Send Email to Editor in Chief">
        <br><br>
    </form>
<br><br>
<? if ($detail['status'] == 2 and $detail['employee_id'] != $this->session->userdata('uid')) { ?>
    <form name="form2" method="post" action="<?php echo site_url('article/decline_process'); ?>" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?= $detail['article_id']; ?>">
        <input type="hidden" name="type" value="<?= $type_id; ?>">
        <input type="hidden" name="status" value="7">
        Decline Reason :<br><textarea name="note" rows="4" cols="50"></textarea>
        <input type=submit value="Decline Article">
        <br><br>
    </form>
<? } ?>

<? } ?>
    <? if ($this->session->userdata('type') == 5 and ($detail['status'] == 3 or $detail['status'] == 9)){ ?><!--CHIEF-->
    <form name="form1" method="post" action=<?= $link; ?> enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?= $detail['article_id']; ?>">
        <input type="hidden" name="type" value="<?= $type_id; ?>">
        <input type="hidden" name="status" value="<?= $status; ?>">
        <input type=submit value="Approve Article And Send Email">
        <br><br>
    </form>
<br><br>
<? if (($detail['status'] == 3 or $detail['status'] == 9) and $detail['employee_id'] != $this->session->userdata('uid')) { ?>
    <form name="form2" method="post" action="<?php echo site_url('article/decline_process') ?>" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?= $detail['article_id']; ?>">
        <input type="hidden" name="type" value="<?= $type_id; ?>">
        <input type="hidden" name="status" value="8">
        Decline Reason :<br><textarea name="note" rows="4" cols="50"></textarea>
        <input type=submit value="Decline Article">
        <br><br>
    </form>
<? } ?>
<? } ?>
    <? if ($this->session->userdata('type') == 6 and ($detail['status'] == 8 or $detail['status'] == 4)){ ?><!--Creative Designer-->
    <form name="form1" method="post" action=<?= $link; ?> enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?= $detail['article_id']; ?>">
        <input type="hidden" name="type" value="<?= $type_id; ?>">
        <input type="hidden" name="status" value="<?= $status; ?>">
        <?php if($total_layouts > 0){ ?>
            <input type=submit value="Approve Article And Send Email">
        <? } else { ?>
            <input type=submit value="Please upload layout first" disabled>
        <? } ?>
        <br><br>
    </form>
<? } ?>
    <input type="button" value="Back" onClick="window.location = '<?php echo site_url('article/approval') ?>'"/>
</div>