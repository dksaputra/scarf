<div class="demo-container">
    <fieldset>
        <legend>Article Photo</legend>
        <section>
            <div id="gallery">
                <ul>
                    <?php $site_url = base_url(); ?>
                    <? foreach ($photo as $data): ?>
                        <li>
                            <? echo "<a href=\"$site_url$data[file]\" target='_blank'>"; ?>
                            <? echo "<img width=200 height=150 src=$site_url$data[file] /></a>"; ?>
                        </li>
                    <? endforeach; ?>
                </ul>
            </div>
        </section>
        <br>
        <td align='center'><a href="<?php echo site_url('article/upload_photo') ?>?id=<?= $id; ?>">UPLOAD PHOTO</a><br>
            <table class="TFtable">
                <tr>
                    <td><b>No</b></td>
                    <td><b>Photo</b></td>
                    <td colspan="2" align="center"><b>Action</b></td>
                </tr>
                <?
                $no = 1;
                $url_delete = site_url('article/delete_photo');
                foreach ($photo as $data3): ?>
                <tr>
                    <td>
                        <?php echo $no; ?>
                    </td>
                    <? echo "<td><img width=50 height=40 src=$site_url$data3[file] /></td>"; ?>
                    <? echo "<td align='center'><a href=\"$site_url$data3[file]\" target='_blank'>View</a></td>" ?>
                    <? echo "<td align='center'><a href=\"$url_delete?id=$data3[photo_article_id]\">Delete</a></td></tr>" ?>
                    <?php
                    $no++;
                    endforeach;
                    ?>
            </table>
    </fieldset>
</div><br>
