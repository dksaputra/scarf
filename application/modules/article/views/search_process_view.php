<div id="content">
    <div class="content-detail">
        <p align="right">
            <strong>Login User : </strong>
            <?php echo $this->session->userdata('nama'); ?><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                Position: </strong><?php echo $this->session->userdata('posisi'); ?>
        </p>
        <table class="TFtable">
            <tr>
                <td><b>Date</b></td>
                <td><b>Author</b></td>
                <td><b>Title</b></td>
                <td><b>Status</b></td>
                <td><b>Last Approval/Decline</b></td>
                <td><b>Note</b></td>
                <? $x = 0; ?>
                <? if ($this->session->userdata('type') == 1) {
                    $x = 6;
                } ?>
                <? if ($this->session->userdata('type') == 2) {
                    $x = 5;
                } ?>
                <? if ($this->session->userdata('type') == 3) {
                    $x = 3;
                } ?>
                <? if ($this->session->userdata('type') == 4) {
                    $x = 3;
                } ?>
                <? if ($this->session->userdata('type') == 5) {
                    $x = 5;
                } ?>
                <? if ($this->session->userdata('type') == 6) {
                    $x = 4;
                } ?>
                <td colspan="<?= $x; ?>" align="center"><b>Action</b></td>
            </tr>
            <font face=arial size=1>
                &nbsp|&nbsp;<a href="<?php echo site_url('article/search') ?>">SEARCH AGAIN</a>&nbsp|&nbsp;
            </font>
            <br><br>
            <strong>Result Searching :</strong>
            <br><br>
            <?php
            $count = 0;
            foreach ($data_articles as $row) {
                $link = "";
                if ($this->session->userdata('type') == 1 and $row['status'] == 0) {
                    $link = "email_editor.php";
                }//reporter-redaktur
                else if ($this->session->userdata('type') == 1 and $row['status'] == 1) {
                    $link = "email_redaktur.php";
                }//reporter-redaktur
                else if ($this->session->userdata('type') == 1 and $row['status'] == 2) {
                    $link = "email_artistik.php";
                }//reporter-redaktur
                else if ($this->session->userdata('type') == 1 and $row['status'] == 3) {
                    $link = "email_editorinchief.php";
                }//reporter-redaktur
                else if ($this->session->userdata('type') == 3) {
                    $link = "email_redaktur.php";
                }//editor-redaktur
                else if ($this->session->userdata('type') == 4) {
                    $link = "email_editorinchief.php";
                }//redaktur-chief
                else if ($this->session->userdata('type') == 5 and $row['status'] == 9) {
                    $link = "email_editorinchief-all.php";
                }//chief to all
                else if ($this->session->userdata('type') == 5) {
                    $link = "email_artistik.php";
                }//chief-Creative Designer
                else if ($this->session->userdata('type') == 6) {
                    $link = "email_editorinchief_layout.php";
                }//aristik-chief

                $index = ($count + 1);
                if ($count % 2 == 1) {
                    $style = "row1";
                } else {
                    $style = "row2";
                }
                echo "<tr class='" . $style . "'>";
                $pecah_tgl_artikel = "";
                $pecah_tgl_artikel = explode("-", $row["article_date"]);
                $tgl_artikel = $pecah_tgl_artikel[2] . "-" . $pecah_tgl_artikel[1] . "-" . $pecah_tgl_artikel[0];
                echo "<td>$tgl_artikel</td>";
                echo "<td>$row[name]</td>";
                echo "<td>$row[title]</td>";
                if ($row['status'] == 0) {
                    $s = "Send Email to Editor First";
                }
                if ($row['status'] == 1) {
                    $s = "Waiting for Editor approval";
                }
                if ($row['status'] == 2) {
                    $s = "Waiting for Redaktur approval";
                }
                if ($row['status'] == 3) {
                    $s = "Waiting for Editor-in Chief approval";
                }
                if ($row['status'] == 4) {
                    $s = "Waiting for Creative Designer approval";
                }
                if ($row['status'] == 5) {
                    $s = "Approved by Editor-in Chief";
                }
                if ($row['status'] == 6) {
                    $s = "<font color='red'>Decline Reporter</font>";
                }
                if ($row['status'] == 7) {
                    $s = "<font color='red'>Decline Editor</font>";
                }
                if ($row['status'] == 8) {
                    $s = "<font color='red'>Decline Creative Designer</font>";
                }
                if ($row['status'] == 9) {
                    $s = "Waiting for Editor-in Chief approval for Layout";
                }
                echo "<td>$s</td>";
                echo "<td>$row[position]</td>";
                echo "<td>$row[note]</td>";
                $url_detail = site_url('article/detail');
                $url_upload = site_url('article/upload_layout');
                $url_edit = site_url('article/edit');
                $url_delete = site_url('article/delete');
                echo "<td align='center'><a href=\"$url_detail?id=$row[article_id]\">VIEW AND SEND</a></td>";
                if ($this->session->userdata('type') == 1) {
                    echo "<td align='center'><a href=\"$url_upload?id=$row[article_id]\">LAYOUT</a></td>";
                    echo "<td align='center'><a href=\"$url_edit?id=$row[article_id]\">EDIT</a></td>";
                    echo "<td align='center'><a href=\"$url_delete?id=$row[article_id]\">DELETE</a></td>";
                }
                /*
                Status Artikel :
                0 Send Email to Editor First
                1 Waiting for Editor approval
                2 Waiting for Redaktur approval
                3 Waiting for Editor-in Chief  approval
                4 Waiting for Artistik approval
                5 Approved by Editor-in Chief
                6 Decline Reporter
                7 Decline Editor
                8 Decline/Approve Artistik
                9 Waiting for Editor-in Chief approval for Layout
                */
                else if ($this->session->userdata('type') == 2 and $row['employee_id'] == $this->session->userdata('uid') and ($row['status'] == 0 or $row['status'] == 6)
                    and $row['status'] != 1 and $row['status'] != 2 and $row['status'] != 3 and $row['status'] != 4
                    and $row['status'] != 5 and $row['status'] != 7 and $row['status'] != 8
                ) {//REPORTER
                    echo "<td align='center'><a href=\"$url_edit?id=$row[article_id]\">EDIT</a></td>";
                    echo "<td align='center'><a href=\"$url_delete?id=$row[article_id]\">DELETE</a></td>";
                } else if ($this->session->userdata('type') == 3 and $row['employee_id'] == $this->session->userdata('uid') and $row['status'] != 0
                    and ($row['status'] == 1 or $row['status'] == 7) and $row['status'] != 2 and $row['status'] != 3 and $row['status'] != 4
                    and $row['status'] != 5 and $row['status'] != 6 and $row['status'] != 8
                ) {//EDITOR
                    echo "<td align='center'><a href=\"$url_edit?id=$row[article_id]\">EDIT</a></td>";
                } else if ($this->session->userdata('type') == 4 and $row['employee_id'] == $this->session->userdata('uid') and $row['status'] != 0
                    and $row['status'] != 1 and $row['status'] == 2 and $row['status'] != 3
                    and $row['status'] != 4 and $row['status'] != 5 and $row['status'] != 6 and $row['status'] != 7
                    and $row['status'] != 8
                ) {//REDAKTUR
                    echo "<td align='center'><a href=\"$url_edit?id=$row[article_id]\">EDIT</a></td>";
                } else if ($this->session->userdata('type') == 5 and $row['employee_id'] == $this->session->userdata('uid') and $row['status'] != 0 and $row['status'] != 1
                    and $row['status'] != 2 and ($row['status'] == 3)
                    and $row['status'] != 5 and $row['status'] != 6 and $row['status'] != 7 and $row['status'] != 8
                ) {//CHIEF
                    echo "<td align='center'><a href=\"$url_edit?id=$row[article_id]\">EDIT</a></td>";
                } else if ($this->session->userdata('type') == 5 and $row['employee_id'] == $this->session->userdata('uid') and $row['status'] != 0 and $row['status'] != 1
                    and $row['status'] != 2 and ($row['status'] == 9)
                    and $row['status'] != 5 and $row['status'] != 6 and $row['status'] != 7 and $row['status'] != 8
                ) {//CHIEF
                    echo "<td align='center'><a href=\"$url_upload?id=$row[article_id]\">LAYOUT</a></td>";
                    echo "<td align='center'><a href=\"$url_edit?id=$row[article_id]\">EDIT</a></td>";
                } else if ($this->session->userdata('type') == 6 and $row['status'] != 0 and $row['status'] != 1
                    and $row['status'] != 2 and ($row['status'] == 4 or $row['status'] == 8)
                    and $row['status'] != 5 and $row['status'] != 6 and $row['status'] != 7
                ) {//Creative Designer
                    echo "<td align='center'><a href=\"$url_upload?id=$row[article_id]\">LAYOUT</a></td>";
                }
                echo "</tr>";
                $count++;
            }
            ?>
        </table>
    </div>
</div>
<tr>
    <td>&nbsp;&nbsp;Total : <b><?php echo $count; ?></b></td>
</tr>
