<div id="content">
    <div class="content-detail">
        <br>
        <form name="delete" method="post" action="<?php echo site_url('article/delete_process') ?>">
            <input type="hidden" name="id" value="<?php echo $detail['article_id']; ?>">
            <table align="center" id="tbl_confirm" width="800" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="center" colspan="2" style="background-color:#dedede;"><b>Konfirmasi Untuk Menghapus Data</b></td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td align="left" width="40%" style="padding-left:10px"><b>Tanggal Artikel :</b></td>
                    <td align="left" width="60%">"<?php echo $detail['article_date'] ?>"</td>
                </tr>
                <tr>
                    <td align="left" width="40%" style="padding-left:10px"><b>Judul Artikel :</b></td>
                    <td align="left" width="60%">"<?php echo $detail['title'] ?>"</td>
                </tr>
                <tr>
                    <td align="left" width="40%" style="padding-left:10px"><b>Isi :</b></td>
                    <td align="left" width="60%">"<?php echo $detail['content'] ?>"</td>
                </tr>
                <tr>
                    <td align="left" width="40%" style="padding-left:10px"><b>Approve Editor :</b></td>
                    <td align="left" width="60%">"<?php echo $detail['name'] ?>"</td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align:center;"><br>Apakah Anda yakin akan menghapus data ini?</td>
                <tr>
                    <td colspan="2" style="text-align:center;"><br>
                        <input type="submit" name="del" value="  Ya  "/>&nbsp;&nbsp;
                        <input type="button" value="Tidak" onClick="self.history.go(-1)"/></a>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="no-border">&nbsp;</td>
                </tr>
            </table>
        </form>

    </div>
</div>
