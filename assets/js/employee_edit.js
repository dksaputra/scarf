var mikExp_no = /[~\\`\\!\\@\\#\\$\\%\\^\\&\\*\\_\\\-\\+\\=\\q\\w\\e\\r\\t\\y\\u\\i\\o\\p\\a\\s\\d\\f\\g\\h\\j\\k\\l\\;\\z\\x\\c\\v\\b\\n\\m\\,\\.\\/\\Q\\W\\E\\R\\T\\Y\\U\\I\\O\\P\\|\\A\\S\\D\\F\\G\\H\\J\\K\\L\\:\\Z\\X\\C\\V\\B\\N\\M\\<\\>\\?\\]/;
function dodacheck_no(val) {
    var strPass = val.value;
    var strLength = strPass.length;
    var lchar = val.value.charAt((strLength) - 1);
    if (lchar.search(mikExp_no) != -1) {
        var tst = val.value.substring(0, (strLength) - 1);
        val.value = tst;
    }
}

var mikExp_char = /[~\\`\\!\\@\\#\\$\\%\\^\\&\\*\\_\\\-\\+\\=\\1\\2\\3\\4\\5\\6\\7\\8\\9\\0\\;\\,\\.\\/\\|\\:\\<\\>\\?\\]/;
function dodacheck_char(val) {
    var strPass = val.value;
    var strLength = strPass.length;
    var lchar = val.value.charAt((strLength) - 1);
    if (lchar.search(mikExp_char) != -1) {
        var tst = val.value.substring(0, (strLength) - 1);
        val.value = tst;
    }
}

function validate() {
    obj = document.form1;
    emp_id = obj.emp_id.value;
    namam = obj.namam.value;
    kelas = obj.kelas.selectedIndex;
    jk = obj.jk.selectedIndex;
    tmptlahir = obj.tmptlahir.value;
    tgl = obj.tgl.selectedIndex;
    bln = obj.bln.selectedIndex;
    thn = obj.thn.selectedIndex;
    asalsekolah = obj.asalsekolah.value;
    alamat = obj.alamat.value;
    telp = obj.telp.value;
    nidns = obj.nidns.selectedIndex;
    submitOK = "True";

    if (emp_id == "") {
        alert("NIK Harus Diisi!")
        obj.emp_id.focus()
        return false;
    }
    if (isNaN(emp_id)) {
        alert("NIK Harus Diisi dengan Angka!")
        obj.emp_id.focus()
        return false;
    }
    if (emp_id.length > 12) {
        alert("NIK tidak boleh lebih dari 12 angka!")
        obj.emp_id.focus()
        return false;
    }
    if (emp_id.length < 12) {
        alert("NIK tidak boleh kurang dari 12 angka!")
        obj.emp_id.focus()
        return false;
    }
    if (namam == "") {
        alert("Nama Harus Diisi!")
        obj.namam.focus()
        return false;
    }
    if (kelas == "") {
        alert("Kelas Harus Diisi!")
        obj.kelas.focus()
        return false;
    }
    if (jk == "") {
        alert("Jenis Kelamin Harus Diisi!")
        obj.jk.focus()
        return false;
    }
    if (tmptlahir == "") {
        alert("Tempat Lahir Harus Diisi!")
        obj.tmptlahir.focus()
        return false;
    }
    if (tgl == 0) {
        alert("Tanggal Lahir Harus Diisi!")
        obj.tgl.focus()
        return false;
    }
    if (bln == 0) {
        alert("Bulan Lahir Harus Diisi!")
        obj.bln.focus()
        return false;
    }
    if (thn == 0) {
        alert("Tahun Lahir Harus Diisi!")
        obj.thn.focus()
        return false;
    }
    if (asalsekolah == "") {
        alert("Asal Sekolah Harus Diisi!")
        obj.asalsekolah.focus()
        return false;
    }
    if (alamat == "") {
        alert("Alamat Harus Diisi!")
        obj.alamat.focus()
        return false;
    }
    if (emp_phone == "") {
        alert("Telp Harus Diisi!")
        obj.emp_phone.focus()
        return false;
    }
    if (isNaN(emp_phone)) {
        alert("Nomor Telp Harus Diisi dengan Angka")
        obj.emp_phone.focus()
        return false;
    }
    if (emp_cellular == "") {
        alert("Telp Harus Diisi!")
        obj.emp_cellular.focus()
        return false;
    }
    if (isNaN(emp_cellular)) {
        alert("Nomor Telp Harus Diisi dengan Angka")
        obj.emp_cellular.focus()
        return false;
    }
    if (nidns == "") {
        alert("NIDN  Harus Diisi!")
        obj.nidns.focus()
        return false;
    }
    if (submitOK == "False") {
        return false;
    }
}/**
 * Created by davidkrisnasaputra on 4/7/16.
 */
