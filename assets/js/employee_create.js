var mikExp_no = /[~\\`\\!\\@\\#\\$\\%\\^\\&\\*\\_\\\-\\+\\=\\q\\w\\e\\r\\t\\y\\u\\i\\o\\p\\a\\s\\d\\f\\g\\h\\j\\k\\l\\;\\z\\x\\c\\v\\b\\n\\m\\,\\.\\/\\Q\\W\\E\\R\\T\\Y\\U\\I\\O\\P\\|\\A\\S\\D\\F\\G\\H\\J\\K\\L\\:\\Z\\X\\C\\V\\B\\N\\M\\<\\>\\?\\]/;
function dodacheck_no(val) {
    var strPass = val.value;
    var strLength = strPass.length;
    var lchar = val.value.charAt((strLength) - 1);
    if (lchar.search(mikExp_no) != -1) {
        var tst = val.value.substring(0, (strLength) - 1);
        val.value = tst;
    }
}

var mikExp_char = /[~\\`\\!\\@\\#\\$\\%\\^\\&\\*\\_\\\-\\+\\=\\1\\2\\3\\4\\5\\6\\7\\8\\9\\0\\;\\,\\.\\/\\|\\:\\<\\>\\?\\]/;
function dodacheck_char(val) {
    var strPass = val.value;
    var strLength = strPass.length;
    var lchar = val.value.charAt((strLength) - 1);
    if (lchar.search(mikExp_char) != -1) {
        var tst = val.value.substring(0, (strLength) - 1);
        val.value = tst;
    }
}

function validate() {
    obj = document.form1;
    emp_id = obj.emp_id.value;
    emp_name = obj.emp_name.value;
    type_id = obj.type_id.selectedIndex;
    emp_gender = obj.emp_gender.selectedIndex;
    emp_marital = obj.emp_marital.selectedIndex;
    emp_lastedu = obj.emp_lastedu.selectedIndex;
    emp_religion = obj.emp_religion.selectedIndex;
    emp_position = obj.emp_position.selectedIndex;
    emp_birthplace = obj.emp_birthplace.value;
    tgl = obj.tgl.selectedIndex;
    bln = obj.bln.selectedIndex;
    thn = obj.thn.selectedIndex;
    emp_address = obj.emp_address.value;
    emp_phone = obj.emp_phone.value;
    emp_cellular = obj.emp_cellular.value;
    emp_email = obj.emp_email.value;
    submitOK = "True";
    if (emp_id == "") {
        alert("NIK Must be Filled!")
        obj.emp_id.focus()
        return false;
    }
    if (isNaN(emp_id)) {
        alert("NIK Must be Filled by Number!")
        obj.emp_id.focus()
        return false;
    }
    if (emp_name == "") {
        alert("Employee Name Must be Filled!")
        obj.emp_name.focus()
        return false;
    }
    if (type_id == "") {
        alert("Type User Must be Filled")
        obj.type_id.focus()
        return false;
    }
    if (emp_gender == "") {
        alert("Sex Must be Filled")
        obj.emp_gender.focus()
        return false;
    }
    if (emp_birthplace == "") {
        alert("Birth Place Must be Filled!")
        obj.emp_birthplace.focus()
        return false;
    }
    if (tgl == 0) {
        alert("Date Must be Filled!")
        obj.tgl.focus()
        return false;
    }
    if (bln == 0) {
        alert("Month Must be Filled!")
        obj.bln.focus()
        return false;
    }
    if (thn == 0) {
        alert("Year Must be Filled!")
        obj.thn.focus()
        return false;
    }
    if (emp_marital == "") {
        alert("Marital Harus Diisi!")
        obj.emp_marital.focus()
        return false;
    }
    if (emp_religion == "") {
        alert("Religion Must be Filled!")
        obj.emp_religion.focus()
        return false;
    }
    if (emp_lastedu == "") {
        alert("Education Must be Filled!")
        obj.emp_lastedu.focus()
        return false;
    }
    if (emp_position == "") {
        alert("Position Must be Filled!")
        obj.emp_position.focus()
        return false;
    }
    if (emp_address == "") {
        alert("Address Must be Filled!")
        obj.emp_address.focus()
        return false;
    }
    if (emp_phone == "") {
        alert("Phone Must be Filled!")
        obj.emp_phone.focus()
        return false;
    }
    if (isNaN(emp_phone)) {
        alert("Phone Must be Filled by Number!")
        obj.emp_phone.focus()
        return false;
    }
    if (emp_cellular == "") {
        alert("Cellular Must be Filled!")
        obj.emp_cellular.focus()
        return false;
    }
    if (isNaN(emp_cellular)) {
        alert("Cellular Must be Filled by Number")
        obj.emp_cellular.focus()
        return false;
    }
    if (emp_email == "") {
        alert("Email Must be Filled!")
        obj.emp_email.focus()
        return false;
    }
    if (submitOK == "False") {
        return false;
    }
}/**
 * Created by davidkrisnasaputra on 4/7/16.
 */
