# ************************************************************
# Sequel Pro SQL dump
# Version 4499
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.10)
# Database: scarf_ci_db
# Generation Time: 2016-04-10 04:56:59 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table tb_articles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tb_articles`;

CREATE TABLE `tb_articles` (
  `article_id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `article_date` date DEFAULT NULL,
  `title` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `content` text CHARACTER SET latin1,
  `file` varchar(150) CHARACTER SET latin1 DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `total_view` int(11) DEFAULT NULL,
  `total_download` int(11) DEFAULT NULL,
  `note` text CHARACTER SET latin1,
  `approve_editor` int(11) DEFAULT NULL,
  `approve_by` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`article_id`),
  KEY `fk_article_employee` (`employee_id`),
  CONSTRAINT `fk_article_employee` FOREIGN KEY (`employee_id`) REFERENCES `tb_employees` (`employee_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tb_articles` WRITE;
/*!40000 ALTER TABLE `tb_articles` DISABLE KEYS */;

INSERT INTO `tb_articles` (`article_id`, `employee_id`, `article_date`, `title`, `content`, `file`, `status`, `total_view`, `total_download`, `note`, `approve_editor`, `approve_by`, `created_by`, `created_date`, `updated_by`, `updated_date`)
VALUES
	(8,2,'2016-02-13','Comfortably Spring','<p>Memasuki musim semi/panas yang santai, H&amp;M mempersembahkan koleksi terbaru yang nyaman dan modis. Tren busana dengan tampilan yang <em>loose</em> dan bahan yang ringan tentu wajib dimiliki musim ini. H&amp;M mewujudkan tren tersebut menjadi item-item yang tak bisa Anda lewatkan. Tersedia kemeja panjang berdraperi seperti piyama lengkap dengan celana padanannya, jaket lilit berbahan brokat, kaftan bercorak kaktus, luaran bercorak burung flamingo atau padanan busana bercorak kulit ular dengan warna yang feminin. Gabungkan dengan esensial ala 70an berupa jeans yang melebar di bawah dan aksesoris topi bucket sebagai pelengkap. Koleksi ini akan tersedia di lebih dari 260 toko di seluruh dunia dan secara online di hm.com mulai tanggal 5 Maret.</p>\r\n','',9,NULL,NULL,'',3,6,NULL,NULL,NULL,'2016-02-13 20:12:44'),
	(9,2,'2016-02-13','Blooming Colors','<p>Memasuki musim panas yang cerah dan ceria, MAC menggandeng <em>high fashion</em> desainer asal Italia, Giambatista Valli untuk berkolaborasi menciptakan varian <em>lipstick</em> terbaru mereka. Terinspirasi dari bunga dan beberapa baju hasil rancangannya sendiri, Giambattista Valli dan MAC menampilkan lima <em>lipstick</em> dengan hasil akhir matte berwarna cantik mulai dari merah muda, merah hingga <em>corals</em>. Tubenya didesain cantik berwarna-warni bukan lagi warna hitam seperti <em>lipstick</em> MAC lainnya. Koleksi ini akan diluncurkan tepatnya pada tanggal 9 Juli 2015, <em>and</em> <em>you should definitely check these beauties</em>!</p>\r\n','',9,NULL,NULL,'',3,6,NULL,NULL,NULL,'2016-02-13 20:18:34'),
	(10,2,'2016-02-13','Blooming Colors','<p>Memasuki musim panas yang cerah dan ceria, MAC menggandeng <em>high fashion</em> desainer asal Italia, Giambatista Valli untuk berkolaborasi menciptakan varian <em>lipstick</em> terbaru mereka. Terinspirasi dari bunga dan beberapa baju hasil rancangannya sendiri, Giambattista Valli dan MAC menampilkan lima <em>lipstick</em> dengan hasil akhir matte berwarna cantik mulai dari merah muda, merah hingga <em>corals</em>. Tubenya didesain cantik berwarna-warni bukan lagi warna hitam seperti <em>lipstick</em> MAC lainnya. Koleksi ini akan diluncurkan tepatnya pada tanggal 9 Juli 2015, <em>and</em> <em>you should definitely check these beauties</em>!</p>\r\n','',9,NULL,NULL,'',4,6,NULL,NULL,NULL,'2016-02-13 20:17:58'),
	(11,2,'2016-02-13','Ekspresi Elegan di Hari Raya','<p>Hari Raya Idul Fitri identik dengan suasana hangat dan bersahaja. Momen yang dinanti oleh seluruh umat muslim di dunia ini tentu saja tidak dilewatkan oleh Zalora Indonesia untuk mempersembahkan koleksi Lebaran 2015. Busana dengan konsep minimalis dan elegan siap menemani Anda bersilaturahmi di hari penuh berkah.</p>\r\n\r\n<p>Sederet desainer <em>muslim wear</em> terbaik memberikan koleksi eksklusif untuk Zalora Indonesia diantaranya adalah Ria Miranda, Jenahara dan Restu Anggraini. Masing-masing desainer mempersembahkan koleksi dalam tema Clair by Ria Miranda for Zalora, Jebaya by Jenahara for Zalora dan Naqiyya by Restu Anggraini for Zalora. Warna pastel kesukaan Ria Miranda dipertahankan dan dibuat dalam item lebih sederhana berupa atasan peplum dan rok lurus, atau sesekali <em>dress </em>panjang bertabur <em>beads</em>. Jenahara membuat kombinasi hitam putih yang minimalis dalam setelan abaya yang lebar dan menjuntai. Sedangkan Restu Anggraini memilih warna dasar putih bernuansa <em>clean</em> menjadi potongan kaftan dan abaya.&nbsp;</p>\r\n\r\n<p>Selain itu turut hadir koleksi dari Rani Hatta, Kami Idea, Monel, Ranti dan Novierock sebagai pilihan lain yang sesuai dengan <em>personal style</em> Anda. Melihat siluet yang feminin namun&nbsp; juga kadang&nbsp;<em>boyish</em>, terlihat koleksi ini pantas dikenakan perempuan dengan segala macam&nbsp; karakter.</p>\r\n\r\n<p>Untuk momen lebih spesial, Zalora Indonesia secara khusus menghadirkan private labelnya bernama Zalia yang selalu hadir dalam gaya <em>classic </em>dan kontemporer. Koleksi terbaru Zalia memiliki unsur glamor abad 19 dengan material mewah berupa lace, sequin, bordir dan balutan ornament berupa beads yang dirangkai cantik diatas busana berupa gaun panjang. Merupakan rangkaian koleksi yang elegan memesona, keseluruhan gaun mewah dari Zalia sangat selaras untuk dipakai dalam perayaan Hari Raya mendatang.</p>\r\n\r\n<p>&nbsp;</p>\r\n','',5,NULL,NULL,'',201601,5,NULL,NULL,NULL,'2016-02-13 20:23:57'),
	(12,2,'2016-02-13','sdasd','<p>sadasd</p>\r\n','',3,NULL,NULL,'',3,4,NULL,NULL,NULL,'2016-02-13 21:09:19'),
	(13,2,'2016-03-28','esese','<p>seses</p>\r\n','',5,NULL,NULL,'',2,5,NULL,NULL,NULL,'2016-03-28 19:44:32'),
	(16,2,'2016-04-04','Minggu','\n\nMinggu\n\n','',0,NULL,NULL,NULL,2,NULL,1,'2016-04-10 11:08:38',NULL,'2016-04-10 11:08:38'),
	(19,1,'2016-04-10','minggu','<p>minggu</p>\n','',0,NULL,NULL,NULL,2,NULL,1,'2016-04-10 11:30:07',NULL,'2016-04-10 11:30:07'),
	(20,1,'2016-04-10','tesatsat','<p>eststest</p>\n','',0,NULL,NULL,NULL,2,NULL,1,'2016-04-10 11:32:31',NULL,'2016-04-10 11:32:31'),
	(21,1,'2016-04-10','ganti','<p>vdzvdzxvdxz</p>\n','',0,NULL,NULL,NULL,2,NULL,1,'2016-04-10 11:33:40',NULL,'2016-04-10 11:38:42');

/*!40000 ALTER TABLE `tb_articles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tb_email_templates
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tb_email_templates`;

CREATE TABLE `tb_email_templates` (
  `email_id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `content` text CHARACTER SET latin1,
  `status` tinyint(1) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`email_id`),
  KEY `type_id` (`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tb_email_templates` WRITE;
/*!40000 ALTER TABLE `tb_email_templates` DISABLE KEYS */;

INSERT INTO `tb_email_templates` (`email_id`, `subject`, `content`, `status`, `type_id`, `created_by`, `created_date`, `updated_by`, `updated_date`)
VALUES
	(1,'Reminder Editor','Dear <b> [approve_editor]</b>,<br/><br/>\n<table border=\"0\">\n<tr>\n  <td width=\"100\" font size=\"5\">Tanggal</font></td>\n  <td font size=\"5\"> : [tgl]  </font></td>\n</tr>\n<tr>\n  <td>Judul</td>\n  <td> : [judul]</td>\n</tr>  \n</table>\n<p align=\"justify\">Artikel tersebut telah selesai diiput dan dicek oleh Reporter. Untuk mengecek dan meng\'approve\' silakan masuk ke sistem.</p><br> \n\nRegards,<br><br>\n<b>Admin</b>',1,1,NULL,NULL,NULL,NULL),
	(2,'Reminder Redaktur','Dear <b> [redaktur]</b>,<br/><br/>\n<table border=\"0\">\n<tr>\n  <td width=\"100\" font size=\"5\">Tanggal</font></td>\n  <td font size=\"5\"> : [tgl]  </font></td>\n</tr>\n<tr>\n  <td>Judul</td>\n  <td> : [judul]</td>\n</tr>  \n</table>\n<p align=\"justify\">Artikel tersebut telah selesai dicek dan diapprove oleh Editor. Untuk mengecek dan meng\'approve\' silakan masuk ke sistem.</p><br> \n\nRegards,<br><br>\n<b>Admin</b>',1,2,NULL,NULL,NULL,NULL),
	(3,'Reminder Artistik','Dear <b> [redaktur]</b>,<br/><br/>\n<table border=\"0\">\n<tr>\n  <td width=\"100\" font size=\"5\">Tanggal</font></td>\n  <td font size=\"5\"> : [tgl]  </font></td>\n</tr>\n<tr>\n  <td>Judul</td>\n  <td> : [judul]</td>\n</tr>  \n</table>\n<p align=\"justify\">Artikel tersebut telah selesai dicek dan diapprove oleh Editor in Chief. Untuk mengecek dan meng\'approve\' silakan masuk ke sistem.</p><br> \n\nRegards,<br><br>\n<b>Admin</b>',1,3,NULL,NULL,NULL,NULL),
	(4,'Reminder Editor-in Chief','Dear <b> [redaktur]</b>,<br/><br/>\n<table border=\"0\">\n<tr>\n  <td width=\"100\" font size=\"5\">Tanggal</font></td>\n  <td font size=\"5\"> : [tgl]  </font></td>\n</tr>\n<tr>\n  <td>Judul</td>\n  <td> : [judul]</td>\n</tr>  \n</table>\n<p align=\"justify\">Artikel tersebut telah selesai dicek dan diapprove oleh Redaktur/Artistik. Untuk mengecek dan meng\'approve\' silakan masuk ke sistem.</p><br> \n\nRegards,<br><br>\n<b>Admin</b>',1,4,NULL,NULL,NULL,NULL),
	(5,'Reminder to All Staff','Dear <b> [redaktur]</b>,<br/><br/>\n<table border=\"0\">\n<tr>\n  <td width=\"100\" font size=\"5\">Tanggal</font></td>\n  <td font size=\"5\"> : [tgl]  </font></td>\n</tr>\n<tr>\n  <td>Judul</td>\n  <td> : [judul]</td>\n</tr>  \n</table>\n<p align=\"justify\">Artikel tersebut telah selesai dicek dan diapprove oleh Editor-in Chief.</p><br> \n\nRegards,<br><br>\n<b>Admin</b>',1,5,NULL,NULL,NULL,NULL),
	(6,'Reminder to All Staff','Dear <b> [redaktur]</b>,<br/><br/>\n<table border=\"0\">\n<tr>\n  <td width=\"100\" font size=\"5\">Tanggal</font></td>\n  <td font size=\"5\"> : [tgl]  </font></td>\n</tr>\n<tr>\n  <td>Judul</td>\n  <td> : [judul]</td>\n</tr>  \n</table>\n<p align=\"justify\">Artikel tersebut telah selesai dicek dan diapprove oleh Editor-in Chief.</p><br> \n\nRegards,<br><br>\n<b>Admin</b>',1,6,NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `tb_email_templates` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tb_employees
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tb_employees`;

CREATE TABLE `tb_employees` (
  `employee_id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL DEFAULT '',
  `birthplace` varchar(45) DEFAULT NULL,
  `birthdate` varchar(20) DEFAULT NULL,
  `gender` varchar(20) DEFAULT NULL,
  `religion` varchar(20) DEFAULT NULL,
  `lastedu` varchar(20) DEFAULT NULL,
  `position` varchar(50) DEFAULT NULL,
  `address` text,
  `phone` text,
  `cellular` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `marital` varchar(20) DEFAULT NULL,
  `photo` varchar(100) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`employee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tb_employees` WRITE;
/*!40000 ALTER TABLE `tb_employees` DISABLE KEYS */;

INSERT INTO `tb_employees` (`employee_id`, `name`, `birthplace`, `birthdate`, `gender`, `religion`, `lastedu`, `position`, `address`, `phone`, `cellular`, `email`, `marital`, `photo`, `status`, `created_by`, `created_date`, `updated_date`, `updated_by`)
VALUES
	(1,'david','jakarta','2-2-1981','Male','Islam','Elementary School','Admin','feafesafeas','434324','423423','fafesafeafesa','Single','uploads/1460262572.png',1,NULL,NULL,'2016-04-10 11:29:32',NULL),
	(2,'asd','jkt','16-2-1993','Male','Islam','Bachelor','Editor','w3w3w','12345','12345','asd','Single','0',1,NULL,NULL,'2016-03-28 19:44:04',NULL),
	(201601,'Temi Sumarlin','Bandung','6-2-1984','Female','Islam','Bachelor','Editor-in Chief','Grand Depok City','0213161916','081227601081','temi.suma@yahoo.com','Married','0',1,NULL,NULL,'2016-02-13 20:04:38',NULL),
	(201602,'Frieska Azaria','Depok','25-4-1992','Female','Islam','Bachelor','Reporter','Depok Jaya Agung','0213161916','081299229881','frieska@scarfmagz','Single','0',1,NULL,NULL,'2016-02-16 16:55:28',NULL),
	(201603,'Nuriy Azizah','Surabaya','10-1-1993','Female','Islam','Bachelor','Reporter','Grand Metroplitan, Pekayon, Bekasi Selatan','0213161916','085692696900','nuriy@scarfmagz','Married','0',1,NULL,NULL,'2016-02-16 16:59:42',NULL),
	(201604,'Fajar Krisdian','Jakarta','29-6-1987','Male','Islam','Bachelor','Creative Designer','Jl. Cipedak, Jagakarsa, Jakarta Selatan','0213161916','087878759783','melehoyoi@gmail','Married','0',1,NULL,NULL,'2016-02-16 17:03:15',NULL),
	(201605,'Anisa Permitia','Semarang','24-11-1991','Female','Islam','High School','Admin','Pondok Gede','0812988881404','0812988881404','anisa@scarfmagz','Single','0',1,NULL,NULL,'2016-03-18 14:11:41',NULL);

/*!40000 ALTER TABLE `tb_employees` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tb_layout_articles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tb_layout_articles`;

CREATE TABLE `tb_layout_articles` (
  `layout_article_id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL,
  `file` varchar(150) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`layout_article_id`),
  KEY `fk_layout_article` (`article_id`),
  CONSTRAINT `fk_layout_article` FOREIGN KEY (`article_id`) REFERENCES `tb_articles` (`article_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tb_layout_articles` WRITE;
/*!40000 ALTER TABLE `tb_layout_articles` DISABLE KEYS */;

INSERT INTO `tb_layout_articles` (`layout_article_id`, `article_id`, `file`, `created_by`, `created_date`, `updated_by`, `updated_date`)
VALUES
	(1,8,'doc1.pdf',6,'2016-02-13 20:33:35',NULL,NULL),
	(2,9,'doc1.pdf',6,'2016-02-13 20:37:29',NULL,NULL),
	(4,10,'doc1.pdf',6,'2016-02-13 20:39:59',NULL,NULL),
	(6,11,'',6,'2016-02-13 20:43:32',NULL,NULL),
	(7,11,'613080048_abstraksi.pdf',6,'2016-03-28 19:49:05',NULL,NULL),
	(8,11,'approval.pdf',6,'2016-03-28 19:49:21',NULL,NULL),
	(9,21,'uploads/1460262900.pdf',1,'2016-04-10 11:35:00',NULL,NULL);

/*!40000 ALTER TABLE `tb_layout_articles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tb_layout_articles_approve
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tb_layout_articles_approve`;

CREATE TABLE `tb_layout_articles_approve` (
  `layout_article_approve_id` int(11) NOT NULL AUTO_INCREMENT,
  `layout_article_id` int(11) NOT NULL,
  `article_id` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`layout_article_approve_id`),
  KEY `fk_layout_approve_layout_article` (`layout_article_id`),
  KEY `fk_layout_approve_article` (`article_id`),
  CONSTRAINT `fk_layout_approve_article` FOREIGN KEY (`article_id`) REFERENCES `tb_articles` (`article_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_layout_approve_layout_article` FOREIGN KEY (`layout_article_id`) REFERENCES `tb_layout_articles` (`layout_article_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tb_layout_articles_approve` WRITE;
/*!40000 ALTER TABLE `tb_layout_articles_approve` DISABLE KEYS */;

INSERT INTO `tb_layout_articles_approve` (`layout_article_approve_id`, `layout_article_id`, `article_id`, `created_by`, `created_date`, `updated_by`, `updated_date`)
VALUES
	(1,7,10,5,'2016-02-13 20:34:57',NULL,NULL),
	(2,8,11,5,'2016-03-28 19:50:27',NULL,NULL),
	(3,9,21,1,'2016-04-10 11:37:11',NULL,NULL);

/*!40000 ALTER TABLE `tb_layout_articles_approve` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tb_photo_articles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tb_photo_articles`;

CREATE TABLE `tb_photo_articles` (
  `photo_article_id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL,
  `file` varchar(150) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`photo_article_id`),
  KEY `fk_photo_article` (`article_id`),
  CONSTRAINT `fk_photo_article` FOREIGN KEY (`article_id`) REFERENCES `tb_articles` (`article_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tb_photo_articles` WRITE;
/*!40000 ALTER TABLE `tb_photo_articles` DISABLE KEYS */;

INSERT INTO `tb_photo_articles` (`photo_article_id`, `article_id`, `file`, `created_by`, `created_date`, `updated_by`, `updated_date`)
VALUES
	(3,8,'uploads/1a2c8d1e9624e2ecd12e395ec0819f85.jpg',2,'2016-02-13 20:13:03',NULL,NULL),
	(4,9,'uploads/af73cb54bae107807727074107fcadae.jpg',3,'2016-02-13 20:17:33',NULL,NULL),
	(5,10,'uploads/54795c64b4e37c446778a1fae3422407.jpg',3,'2016-02-13 20:17:58',NULL,NULL),
	(6,11,'uploads/d93a8c7b9d0c1ebc480c18a369331f42.jpg',4,'2016-02-13 20:23:57',NULL,NULL),
	(9,11,'uploads/e8a3432b57b32552ee04e9415f6d559b.jpg',2,'2016-02-13 21:09:19',NULL,NULL),
	(10,11,'uploads/4c29a2453d9b47d2002bf0b1dc3f3f0d.jpg',2,'2016-03-28 19:44:33',NULL,NULL),
	(11,13,'uploads/1460261062.jpg',1,'2016-04-10 11:04:22',NULL,NULL),
	(12,16,'uploads/1460262187.jpg',201601,'2016-04-10 11:23:07',NULL,NULL),
	(13,20,'uploads/1460262751.png',1,'2016-04-10 11:32:31',NULL,NULL),
	(14,21,'uploads/1460262820.png',1,'2016-04-10 11:33:40',NULL,NULL);

/*!40000 ALTER TABLE `tb_photo_articles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tb_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tb_users`;

CREATE TABLE `tb_users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` int(11) NOT NULL,
  `password` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `email` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT '',
  `name` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT '',
  `type_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  KEY `fk_user_email` (`type_id`),
  KEY `fk_user_employee` (`username`),
  CONSTRAINT `fk_user_email` FOREIGN KEY (`type_id`) REFERENCES `tb_email_templates` (`type_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_user_employee` FOREIGN KEY (`username`) REFERENCES `tb_employees` (`employee_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tb_users` WRITE;
/*!40000 ALTER TABLE `tb_users` DISABLE KEYS */;

INSERT INTO `tb_users` (`user_id`, `username`, `password`, `email`, `name`, `type_id`, `created_by`, `created_date`, `updated_by`, `updated_date`)
VALUES
	(7,201601,'13174a5ae720f555c82452f457f25f39','rochmanaw@yahoo.com','Temi Sumarlin',6,NULL,NULL,NULL,NULL),
	(8,201602,'d8c5bb17b7dbe74378d67fe7fb564917','rochmanaw@yahoo.com','Frieska Azaria',2,NULL,NULL,NULL,NULL),
	(9,201603,'fc84b3c024524e86314f9dc6cf849163','rochmanaw@yahoo.com','Nuriy Azizah',2,NULL,NULL,NULL,NULL),
	(10,201604,'ef367ef7733b0421f0c00f2e72c8de74','rochmanaw@yahoo.com','Fajar Krisdian',5,NULL,NULL,NULL,NULL),
	(11,201605,'166d6987a8bd5b2275f8de3f206bcb04','rochmanaw@yahoo.com','Anisa Permitia',1,NULL,NULL,NULL,NULL),
	(14,1,'c4ca4238a0b923820dcc509a6f75849b','','david',1,NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `tb_users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
